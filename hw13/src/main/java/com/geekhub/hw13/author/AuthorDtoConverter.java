package com.geekhub.hw13.author;

import org.springframework.stereotype.Component;

/**
 * This component is used to convert entity objects to dto objects
 */
@Component
public class AuthorDtoConverter {

    public AuthorDto convert(Author author) {
        AuthorDto authorDto = new AuthorDto();
        authorDto.setId(author.getId());
        authorDto.setName(author.getName());
        authorDto.setGender(GenderDto.valueOf(author.getGender().name()));
        return authorDto;
    }

    public AuthorWithMovieCountDto convert(Author author, int movieCount) {
        AuthorWithMovieCountDto authorWithMovieCountDto = new AuthorWithMovieCountDto();
        authorWithMovieCountDto.setMovieCount(movieCount);
        authorWithMovieCountDto.setId(author.getId());
        authorWithMovieCountDto.setName(author.getName());
        authorWithMovieCountDto.setGender(GenderDto.valueOf(author.getGender().name()));
        return authorWithMovieCountDto;
    }
}
