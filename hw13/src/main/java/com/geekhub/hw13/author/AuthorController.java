package com.geekhub.hw13.author;

import com.geekhub.hw13.authormovie.AuthorMovieRelation;
import com.geekhub.hw13.authormovie.AuthorMovieRelationService;
import com.geekhub.hw13.downloadcontent.ExportService;
import com.geekhub.hw13.movie.Movie;
import com.geekhub.hw13.movie.MovieDto;
import com.geekhub.hw13.movie.MovieDtoConverter;
import com.geekhub.hw13.movie.MovieService;
import com.itextpdf.text.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/authors")
public class AuthorController {

    private final AuthorService authorService;
    private final AuthorMovieRelationService authorMovieRelationService;
    private final MovieService movieService;
    private final AuthorDtoConverter authorDtoConverter;
    private final MovieDtoConverter movieDtoConverter;
    private final ExportService exportService;


    @Autowired
    public AuthorController(AuthorService authorService,
                            AuthorMovieRelationService authorMovieRelationService,
                            MovieService movieService,
                            AuthorDtoConverter authorDtoConverter,
                            MovieDtoConverter movieDtoConverter, ExportService exportService) {
        this.authorService = authorService;
        this.authorMovieRelationService = authorMovieRelationService;
        this.movieService = movieService;
        this.authorDtoConverter = authorDtoConverter;
        this.movieDtoConverter = movieDtoConverter;
        this.exportService = exportService;
    }

    @GetMapping
    public String getIndexPage(ModelMap model) {
        List<AuthorWithMovieCountDto> authors = new ArrayList<>();
        for (Author author : authorService.getAllAuthors()) {
            Map<Integer, Integer> movieCountByAuthorId = authorService.getMovieCountByAuthorId();
            Integer movieCount = movieCountByAuthorId.get(author.getId());
            AuthorWithMovieCountDto authorWithMovieCountDto;
            if (!(movieCount == null)) {
                authorWithMovieCountDto = authorDtoConverter.convert(author, movieCount);
            } else {
                authorWithMovieCountDto = authorDtoConverter.convert(author, 0);
            }
            authors.add(authorWithMovieCountDto);
        }
        model.addAttribute("authors", authors);
        return "author/index";
    }

    @GetMapping("/create")
    public String getCreatePage(ModelMap model) {
        model.addAttribute("genders", GenderDto.values());
        return "author/create";
    }

    @PostMapping("/create")
    public String createAuthor(@RequestParam String name,
                               @RequestParam GenderDto gender) {
        Author author = authorService.createAuthor(name, Gender.valueOf(gender.name()));
        return "redirect:/authors/" + author.getId() + "/show";
    }

    @GetMapping("/{authorId}/show")
    public String getShowAuthorPage(@PathVariable int authorId, ModelMap model) {
        Author author = authorService.getAuthorById(authorId);
        author.setId(authorId);
        AuthorDto authorDto = authorDtoConverter.convert(author);
        List<AuthorMovieRelation> authorMovieRelationByAuthorId =
                authorMovieRelationService.getAuthorMovieRelationByAuthorId(authorId);
        List<Integer> movieIds = new ArrayList<>();
        for (AuthorMovieRelation authorMovieRelation : authorMovieRelationByAuthorId) {
            movieIds.add(authorMovieRelation.getMovieId());
        }
        List<Movie> moviesByIds = movieService.getMoviesByIds(movieIds);
        List<MovieDto> movieDtoList = new ArrayList<>();
        for (Movie moviesById : moviesByIds) {
            movieDtoList.add(movieDtoConverter.convert(moviesById));
        }
        model.addAttribute("author", authorDto);
        model.addAttribute("movies", movieDtoList);
        return "author/show";
    }

    @GetMapping("/doc")
    public ResponseEntity<byte[]> downloadWord(){
        String sb = getAuthorsInStringFormat();

        byte[] bytes = sb.getBytes();
        HttpHeaders headers = new HttpHeaders();
        headers.set("charset", "utf-8");
        headers.setContentType(MediaType.TEXT_HTML);
        headers.setContentLength(bytes.length);
        headers.set("Content-disposition", "attachment; filename = authors.doc");

        return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
    }

    @GetMapping("/pdf")
    public void downloadPdf(HttpServletResponse response) throws DocumentException, IOException {
        String authors = getAuthorsInStringFormat();
        String filename = "authors.pdf";
        byte[] pdf = exportService.preparePdf(authors, new Document());
        exportService.downloadFile(response, pdf, filename, MediaType.APPLICATION_PDF);

    }

    @GetMapping("/excel")
    public void downloadExcel(HttpServletResponse response) throws IOException {
        List<Author> allAuthors = authorService.getAllAuthors();
        String filename = "authors.xls";
        MediaType mediaType = MediaType.valueOf("application/vnd.ms-excel");
        byte[] preparedExcel = exportService.prepareExcel(allAuthors);
        exportService.downloadFile(response, preparedExcel, filename, mediaType);
    }

    private String getAuthorsInStringFormat() {
        StringBuilder sb = new StringBuilder();
        List<Author> allAuthors = authorService.getAllAuthors();
        for (Author author : allAuthors) {
            sb.append(author.toString());
        }
        return sb.toString();
    }
}
