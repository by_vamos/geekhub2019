package com.geekhub.hw13.downloadcontent;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

@Service
public class ExportService {

    public byte[] preparePdf(String content, Document document)
            throws DocumentException, IOException{
        byte[] contentBytes;
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            PdfWriter.getInstance(document, outputStream);
            document.open();
            Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
            document.add(new Paragraph(content, font));
            document.close();
            contentBytes = outputStream.toByteArray();
        }

        return contentBytes;
    }

    public byte[] prepareExcel(List contentList) throws IOException {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet();
        int rowCount = 0;
        byte[] contentBytes;
        for (Object content : contentList) {
            String s = content.toString();
            Row row = sheet.createRow(rowCount++);
            Cell cell = row.createCell(1);
            cell.setCellValue(s);
        }
        try (ByteArrayOutputStream output = new ByteArrayOutputStream()) {
            workbook.write(output);
            contentBytes = output.toByteArray();
        }
        return contentBytes;
    }

    public void downloadFile(HttpServletResponse response, byte[] contentBytes, String filename, MediaType mediaType) throws IOException {
        response.setContentType(mediaType.getType());
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename);
        response.setContentLength(contentBytes.length);

        try (BufferedInputStream inStream = new BufferedInputStream(new ByteArrayInputStream(contentBytes))) {
            try (BufferedOutputStream outStream = new BufferedOutputStream(response.getOutputStream())) {

                byte[] buffer = new byte[1024];
                int bytesRead;
                while ((bytesRead = inStream.read(buffer)) != -1) {
                    outStream.write(buffer, 0, bytesRead);
                }
                outStream.flush();
            }
        }
    }

}
