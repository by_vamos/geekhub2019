package com.geekhub.hw13.moviefeedbacks;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class FeedbackRowMapper implements RowMapper<Feedback> {

    @Override
    public Feedback mapRow(ResultSet rs, int rowNum) throws SQLException {
        Feedback feedback = new Feedback();
        feedback.setDate(rs.getString("datetime"));
        feedback.setName(rs.getString("name"));
        feedback.setMessage(rs.getString("message"));
        feedback.setRating(rs.getString("rating"));

        return feedback;
    }
}
