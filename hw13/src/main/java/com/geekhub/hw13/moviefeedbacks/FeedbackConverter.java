package com.geekhub.hw13.moviefeedbacks;

import org.springframework.stereotype.Component;

@Component
public class FeedbackConverter {

    public FeedbackDto convert(Feedback feedback) {
        FeedbackDto feedbackDto = new FeedbackDto();
        feedbackDto.setDate(feedback.getDate());
        feedbackDto.setName(feedback.getName());
        feedbackDto.setMessage(feedback.getMessage());
        feedbackDto.setRating(feedback.getRating());
        return feedbackDto;
    }
}
