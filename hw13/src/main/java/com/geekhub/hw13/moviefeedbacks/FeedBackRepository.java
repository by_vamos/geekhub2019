package com.geekhub.hw13.moviefeedbacks;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FeedBackRepository {
    private final FeedbackRowMapper feedbackRowMapper;
    private final NamedParameterJdbcTemplate jdbcTemplate;

    public FeedBackRepository(FeedbackRowMapper feedbackRowMapper, NamedParameterJdbcTemplate jdbcTemplate) {
        this.feedbackRowMapper = feedbackRowMapper;
        this.jdbcTemplate = jdbcTemplate;
    }


    public void saveFeedback(Feedback feedback, int movie_id) {
        String sql = "INSERT INTO movie_feedbacks (movie_id, datetime, name, message, rating) " +
                "VALUES (:movie_id, :datetime, :name, :message, :rating)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("movie_id", movie_id);
        params.addValue("datetime", feedback.getDate());
        params.addValue("name", feedback.getName());
        params.addValue("message", feedback.getMessage());
        params.addValue("rating", Integer.valueOf(feedback.getRating()));

        jdbcTemplate.update(sql, params);
    }

    public List<Feedback> getAllFeedbacksByMovieId(int movie_id) {
        String sql = "SELECT id, movie_id, datetime, name, message, rating FROM movie_feedbacks WHERE movie_id = " + movie_id;
        return jdbcTemplate.query(sql, feedbackRowMapper);
    }

    public Integer getFeedbacksCountByMovieId(int movie_id) {
        String sql ="SELECT count(id) FROM movie_feedbacks WHERE movie_id = :movie_id GROUP BY movie_id";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("movie_id", movie_id);
        try {
            return jdbcTemplate.queryForObject(sql, params, Integer.class);
        } catch (EmptyResultDataAccessException e) {
            return 0;
        }
    }

    public Double getAvgRatingByMovieId(int movie_id) {
        String sql ="SELECT avg(rating) FROM movie_feedbacks WHERE movie_id = :movie_id GROUP BY movie_id";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("movie_id", movie_id);
        try {
            return jdbcTemplate.queryForObject(sql, params, Double.class);
        } catch (EmptyResultDataAccessException e) {
            return 0.0;
        }
    }
}
