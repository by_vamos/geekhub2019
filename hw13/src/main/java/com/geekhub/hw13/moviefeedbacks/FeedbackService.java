package com.geekhub.hw13.moviefeedbacks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class FeedbackService {

    private final FeedBackRepository feedBackRepository;

    @Autowired
    public FeedbackService(FeedBackRepository feedBackRepository) {
        this.feedBackRepository = feedBackRepository;
    }

    public void saveFeedback(String name, String message, String rating, int movie_id) {
        String time = LocalTime.now().format(DateTimeFormatter.ofPattern("hh:mm:ss")) + " " +
                LocalDate.now().getMonth() + " " + LocalDate.now().getDayOfMonth();
        Feedback feedback = new Feedback();
        feedback.setDate(time);
        feedback.setName(name);
        feedback.setMessage(message);
        feedback.setRating(rating);
        feedBackRepository.saveFeedback(feedback, movie_id);
    }

    public List<Feedback> getAllFeedbacksByMovieId(int movie_id) {
        return feedBackRepository.getAllFeedbacksByMovieId(movie_id);
    }

    public Integer getFeedbacksCountByMovieId(int movie_id) {
        return feedBackRepository.getFeedbacksCountByMovieId(movie_id);
    }

    public Double getAvgRatingByMovieId(int movie_id) {
        return feedBackRepository.getAvgRatingByMovieId(movie_id);
    }
}
