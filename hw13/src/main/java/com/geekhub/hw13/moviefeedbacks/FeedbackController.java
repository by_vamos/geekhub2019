package com.geekhub.hw13.moviefeedbacks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FeedbackController {

    private final FeedbackService feedBackService;

    @Autowired
    public FeedbackController(FeedbackService feedBackService) {
        this.feedBackService = feedBackService;
    }

    @PostMapping("/movies/{movieId}/show")
    public String form(@RequestParam(required = false) String name,
                       @RequestParam(required = false) String message,
                       @RequestParam(required = false) String rating,
                       @PathVariable int movieId) {
        feedBackService.saveFeedback(name, message, rating, movieId);
        return "redirect:/movies/" + movieId + "/show";
    }
}
