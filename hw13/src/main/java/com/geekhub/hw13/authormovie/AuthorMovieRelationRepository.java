package com.geekhub.hw13.authormovie;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AuthorMovieRelationRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final AuthorMovieRelationRowMapper authorMovieRelationRowMapper;

    @Autowired
    public AuthorMovieRelationRepository(NamedParameterJdbcTemplate jdbcTemplate,
                                         AuthorMovieRelationRowMapper authorMovieRelationRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.authorMovieRelationRowMapper = authorMovieRelationRowMapper;
    }

    public void saveAuthorMovieRelation(AuthorMovieRelation authorMovieRelation) {
        String sql = "INSERT INTO author_movie_relation (author_id, movie_id) VALUES (:author_id, :movie_id)";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("author_id", authorMovieRelation.getAuthorId());
        params.addValue("movie_id", authorMovieRelation.getMovieId());
        jdbcTemplate.update(sql, params);
    }

    public Map<Integer, Integer> getMovieCountByAuthorId() {
        String sql = "SELECT author_id, count(id) FROM author_movie_relation GROUP BY author_id";
        return jdbcTemplate.query(sql, (rs) -> {
            HashMap<Integer, Integer> map = new HashMap<>();
            while (rs.next()) {
                map.put(rs.getInt("author_id"), rs.getInt("count"));
            }
            return map;
        });
    }

    public List<AuthorMovieRelation> getAuthorMovieRelationByMovieId(int movieId) {
        String sql = "SELECT author_id, movie_id FROM author_movie_relation WHERE movie_id = " + movieId;
        return jdbcTemplate.query(sql, authorMovieRelationRowMapper);
    }

    public List<AuthorMovieRelation> getAuthorMovieRelationByAuthorId(int authorId) {
        String sql = "SELECT author_id, movie_id FROM author_movie_relation WHERE author_id = " + authorId;
        return jdbcTemplate.query(sql, authorMovieRelationRowMapper);
    }
}
