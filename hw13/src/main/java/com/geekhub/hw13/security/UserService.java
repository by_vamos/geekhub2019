package com.geekhub.hw13.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void saveUser(String name, String password) {
        User user = new User();
        user.setName(name);
        user.setPassword(password);
        userRepository.saveUser(user);
    }
}
