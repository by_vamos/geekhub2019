package com.geekhub.hw13.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class WebSecurityController {

    private final UserService userService;

    @Autowired
    public WebSecurityController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/login")
    public String form() {
        return "login";
    }

    @PostMapping("/register")
    public String form(@RequestParam String username,
                     @RequestParam String password) {
        userService.saveUser(username, password);
        return "redirect:/movies";
    }

    @GetMapping("/register")
    public String reg() {
        return "register";
    }

}
