package com.geekhub.hw13.movie;

public enum Genre {
    COMEDY, ACTION, PARODY
}
