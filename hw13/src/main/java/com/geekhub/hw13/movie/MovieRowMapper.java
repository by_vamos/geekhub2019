package com.geekhub.hw13.movie;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class MovieRowMapper implements RowMapper<Movie> {

    @Override
    public Movie mapRow(ResultSet rs, int rowNum) throws SQLException {
        Movie movie = new Movie();
        movie.setId(rs.getInt("id"));
        movie.setName(rs.getString("name"));
        movie.setYear(rs.getInt("year"));
        movie.setGenre(Genre.valueOf(rs.getString("genre")));
        movie.setLanguage(Language.valueOf(rs.getString("language")));
        try {
            movie.setCommentaryCount(rs.getInt("count"));
        } catch (SQLException ex) {
            movie.setCommentaryCount(0);
        }
        try {
            movie.setRating(rs.getDouble("avg"));
        } catch (SQLException ex) {
            movie.setRating(0.0);
        }

        return movie;
    }
}
