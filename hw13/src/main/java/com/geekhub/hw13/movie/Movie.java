package com.geekhub.hw13.movie;

public class Movie {

    private Integer id;
    private String name;
    private int year;
    private Genre genre;
    private Language language;
    private int commentaryCount;
    private double rating;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public int getCommentaryCount() {
        return commentaryCount;
    }

    public void setCommentaryCount(int commentaryCount) {
        this.commentaryCount = commentaryCount;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", year=" + year +
                ", genre=" + genre +
                ", language=" + language +
                ", commentaryCount=" + commentaryCount +
                ", rating=" + rating +
                "} ";
    }
}
