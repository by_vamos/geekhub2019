package com.geekhub.hw13.movie;

public enum Language {
    ENGLISH, SPANISH, UKRAINIAN
}
