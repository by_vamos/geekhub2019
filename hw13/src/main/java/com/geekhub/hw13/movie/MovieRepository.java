package com.geekhub.hw13.movie;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MovieRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final MovieRowMapper movieRowMapper;

    @Autowired
    public MovieRepository(NamedParameterJdbcTemplate jdbcTemplate, MovieRowMapper movieRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.movieRowMapper = movieRowMapper;
    }

    public Movie saveMovie(Movie movie) {
        String sql = "INSERT INTO movie (name, year, genre, language) VALUES (:name, :year, :genre, :language) RETURNING id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", movie.getName());
        params.addValue("year", movie.getYear());
        params.addValue("genre", movie.getGenre().name());
        params.addValue("language", movie.getLanguage().name());

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(sql, params, keyHolder);

        int generatedId = keyHolder.getKey().intValue();

        movie.setId(generatedId);

        return movie;
    }

    public List<Movie> getAllMovies() {
        String sql = "SELECT m.id, m.name, year, genre, language, count(mf.id), avg(mf.rating)\n" +
                "FROM movie m\n" +
                "LEFT JOIN movie_feedbacks mf on m.id = mf.movie_id\n" +
                "GROUP BY m.id, mf.movie_id\n" +
                "ORDER BY m.id";
        return jdbcTemplate.query(sql, movieRowMapper);
    }

    public Movie getMovieById(int movieId) {
        String sql = "SELECT id, name, year, genre, language FROM movie WHERE id = :id";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", movieId);

        return jdbcTemplate.queryForObject(sql, params, movieRowMapper);
    }

    public List<Movie> getMoviesByIds(List<Integer> movieIds) {
        String sql = "SELECT id, name, year, genre, language FROM movie WHERE id IN (:movieIds)";

        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("movieIds", movieIds);

        return jdbcTemplate.query(sql, params, movieRowMapper);
    }
}
