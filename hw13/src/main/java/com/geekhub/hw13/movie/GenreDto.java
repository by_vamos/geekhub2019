package com.geekhub.hw13.movie;

public enum GenreDto {
    COMEDY, ACTION, PARODY
}
