package com.geekhub.hw13.movie;

import com.geekhub.hw13.author.Author;
import com.geekhub.hw13.author.AuthorDto;
import com.geekhub.hw13.author.AuthorDtoConverter;
import com.geekhub.hw13.author.AuthorService;
import com.geekhub.hw13.authormovie.AuthorMovieRelation;
import com.geekhub.hw13.authormovie.AuthorMovieRelationService;
import com.geekhub.hw13.downloadcontent.ExportService;
import com.geekhub.hw13.moviefeedbacks.Feedback;
import com.geekhub.hw13.moviefeedbacks.FeedbackConverter;
import com.geekhub.hw13.moviefeedbacks.FeedbackDto;
import com.geekhub.hw13.moviefeedbacks.FeedbackService;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/movies")
public class MovieController {

    private final MovieService movieService;
    private final AuthorService authorService;
    private final AuthorMovieRelationService authorMovieRelationService;
    private final MovieDtoConverter movieDtoConverter;
    private final AuthorDtoConverter authorDtoConverter;
    private final FeedbackService feedbackService;
    private final FeedbackConverter feedbackConverter;
    private final ExportService exportService;

    @Autowired
    public MovieController(MovieService movieService,
                           AuthorService authorService,
                           AuthorMovieRelationService authorMovieRelationService,
                           MovieDtoConverter movieDtoConverter,
                           AuthorDtoConverter authorDtoConverter, FeedbackService feedbackService, FeedbackConverter feedbackConverter, ExportService exportService) {
        this.movieService = movieService;
        this.authorService = authorService;
        this.authorMovieRelationService = authorMovieRelationService;
        this.movieDtoConverter = movieDtoConverter;
        this.authorDtoConverter = authorDtoConverter;
        this.feedbackService = feedbackService;
        this.feedbackConverter = feedbackConverter;
        this.exportService = exportService;
    }

    @GetMapping
    public String getIndexPage(ModelMap model) {
        List<MovieDto> movieDtosList = new ArrayList<>();
        for (Movie movie : movieService.getAllMovies()) {
            Integer movieId = movie.getId();
            Integer commentaryCount = feedbackService.getFeedbacksCountByMovieId(movieId);
            Double avgRating = feedbackService.getAvgRatingByMovieId(movieId);
            movie.setCommentaryCount(commentaryCount);
            movie.setRating(avgRating);
            MovieDto movieDto = movieDtoConverter.convert(movie);
            movieDtosList.add(movieDto);
        }
        model.addAttribute("movies", movieDtosList);
        return "movie/index";
    }

    @GetMapping("/create")
    public String getCreatePage(ModelMap model) {
        model.addAttribute("genres", GenreDto.values());
        model.addAttribute("languages", LanguageDto.values());
        model.addAttribute("authorIds", authorService.getAllAuthors());
        return "movie/create";
    }

    @PostMapping("/create")
    public String createMovie(@RequestParam String name,
                              @RequestParam int year,
                              @RequestParam GenreDto genre,
                              @RequestParam LanguageDto language,
                              @RequestParam List<Integer> authorIds) {
        Movie movie = movieService.createMovie(name,
                year,
                Genre.valueOf(genre.name()),
                Language.valueOf(language.name()),
                authorIds);
        return "redirect:/movies/" + movie.getId() + "/show";
    }

    @GetMapping("/{movieId}/show")
    public String getShowMoviePage(@PathVariable int movieId, ModelMap model) {
        Movie movie = movieService.getMovieById(movieId);
        List<AuthorMovieRelation> authorMovieRelationByMovieId =
                authorMovieRelationService.getAuthorMovieRelationByMovieId(movieId);
        List<Integer> authorIds = new ArrayList<>();
        for (AuthorMovieRelation authorMovieRelation : authorMovieRelationByMovieId) {
            int authorId = authorMovieRelation.getAuthorId();
            authorIds.add(authorId);
        }

        List<Author> authorById = authorService.getAuthorById(authorIds);
        List<AuthorDto> authorsDtoList = new ArrayList<>();
        for (Author author : authorById) {
            AuthorDto authorDto = authorDtoConverter.convert(author);
            authorsDtoList.add(authorDto);
        }
        MovieDto movieDto = movieDtoConverter.convert(movie);

        List<Feedback> allFeedbacksByMovieId = feedbackService.getAllFeedbacksByMovieId(movieId);
        List<FeedbackDto> feedbackDtoList = new ArrayList<>();
        for (Feedback feedback : allFeedbacksByMovieId) {
            FeedbackDto feedbackDto = feedbackConverter.convert(feedback);
            feedbackDtoList.add(feedbackDto);
        }

        model.addAttribute("movie", movieDto);
        model.addAttribute("authors", authorsDtoList);
        model.addAttribute("feedbacks", feedbackDtoList);
        return "movie/show";
    }

    @GetMapping("/doc")
    public ResponseEntity<byte[]> downloadWord() {
        String sb = getMoviesInStringFormat();
        byte[] bytes = sb.getBytes();
        HttpHeaders headers = new HttpHeaders();
        headers.set("charset", "utf-8");
        headers.setContentType(MediaType.TEXT_HTML);
        headers.setContentLength(bytes.length);
        headers.set("Content-disposition", "attachment; filename = movies.doc");
        return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
    }

    @GetMapping("/pdf")
    public void downloadPdf(HttpServletResponse response) throws DocumentException, IOException {
        String movies = getMoviesInStringFormat();
        String filename = "movies.pdf";
        byte[] preparedPdf = exportService.preparePdf(movies, new Document());
        exportService.downloadFile(response, preparedPdf, filename, MediaType.APPLICATION_PDF);
    }

    @GetMapping("/excel")
    public void downloadExcel(HttpServletResponse response) throws IOException {
        List<Movie> allMovies = movieService.getAllMovies();
        String filename = "movies.xls";
        MediaType mediaType = MediaType.valueOf("application/vnd.ms-excel");
        byte[] preparedExcel = exportService.prepareExcel(allMovies);
        exportService.downloadFile(response, preparedExcel, filename, mediaType);
    }

    private String getMoviesInStringFormat() {
        StringBuilder sb = new StringBuilder();
        List<Movie> allMovies = movieService.getAllMovies();
        for (Movie movie : allMovies) {
            sb.append(movie.toString());
        }
        return sb.toString();
    }
}
