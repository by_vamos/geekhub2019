package com.geekhub.hw13.movie;

public enum LanguageDto {
    ENGLISH, SPANISH, UKRAINIAN
}
