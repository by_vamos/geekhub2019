package com.geekhub.hw13.movie;

import org.springframework.stereotype.Component;

@Component
public class MovieDtoConverter {

    public MovieDto convert(Movie movie) {
        MovieDto movieDto = new MovieDto();
        movieDto.setId(movie.getId());
        movieDto.setName(movie.getName());
        movieDto.setYear(movie.getYear());
        movieDto.setGenre(GenreDto.valueOf(movie.getGenre().name()));
        movieDto.setLanguage(LanguageDto.valueOf(movie.getLanguage().name()));
        movieDto.setCommentaryCount(movie.getCommentaryCount());
        movieDto.setRating(movie.getRating());
        return movieDto;
    }
}
