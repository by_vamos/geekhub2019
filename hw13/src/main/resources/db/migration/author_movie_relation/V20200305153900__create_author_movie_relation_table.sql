CREATE TABLE author_movie_relation
(
    id SERIAL PRIMARY KEY,
    author_id INT NOT NULL,
    movie_id INT NOT NULL,
    foreign key (author_id) references author (id),
    foreign key (movie_id) references movie (id)
);