CREATE TABLE movie
(
    id       SERIAL PRIMARY KEY,
    name     VARCHAR NOT NULL,
    year     INT NOT NULL,
    genre    VARCHAR NOT NULL,
    language VARCHAR NOT NULL
);