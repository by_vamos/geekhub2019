CREATE TABLE movie_feedbacks
(
    id SERIAL PRIMARY KEY,
    movie_id INT,
    datetime VARCHAR NOT NULL,
    name VARCHAR,
    message VARCHAR,
    rating VARCHAR,
    FOREIGN KEY (movie_id) references movie (id)
)