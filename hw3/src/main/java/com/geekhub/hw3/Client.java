package com.geekhub.hw3;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Objects;

class Client {
    private String name;
    private int age;
    private double average_income;
    private ArrayList<Deposit> deposits = new ArrayList<>();
    private ArrayList<Account> accounts = new ArrayList<>();
    private ArrayList<Storage> storages = new ArrayList<>();
    private double total_amount = 0;

    Client(String name, int age, double average_income) {
        this.name = name;
        this.age = age;
        this.average_income = average_income;
    }

    public String getName() {
        return name;
    }

    ArrayList<Deposit> getDeposits() {
        return deposits;
    }

    ArrayList<Account> getAccounts() {
        return accounts;
    }

    ArrayList<Storage> getStorages() {
        return storages;
    }

    double getTotal_amount() {
        return total_amount;
    }

    private Deposit getDepositByCurrency(Currency currency) {
        for (Deposit deposit : deposits) {
            if (deposit.getCurrency() == currency) {
                return deposit;
            }
        }
        return null;
    }

    private Account getAccountByCurrency(Currency currency) {
        for (Account account : accounts) {
            if (account.getCurrency() == currency) {
                return account;
            }
        }
        return null;
    }

    private Storage getStorageByCurrency(Currency currency) {
        for (Storage storage : storages) {
            if (storage.getType_of_metal() == currency) {
                return storage;
            }
        }
        return null;
    }

    String addMoneyOnDepositByCurrency(int value, Currency currency) {
        Deposit deposit = getDepositByCurrency(currency);
        if (deposit != null) {
            deposit.addMoney(value);
            return "Thank you. Add " + value + " " + currency + " in your deposit";
        } else {
            return "You doesn't have " + currency + " deposit";
        }
    }

    String addMoneyOnAccountByCurrency(int value, Currency currency) {
        Account account = getAccountByCurrency(currency);
        if (account != null) {
            account.addMoney(value);
            return "Thank you. Add " + value + " " + currency + " in your account";
        } else {
            return "You doesn't have " + currency + " account";
        }
    }

    String addMoneyInStorageByCurrency(int value, Currency currency) {
        Storage storage = getStorageByCurrency(currency);
        if (storage != null) {
            storage.addMoney(value);
            return "Thank you. Add " + value + " Kg of " + currency + " in your storage";
        } else {
            return "You doesn't have " + currency + " storage";
        }
    }

    String withdrawnMoneyByCurrency(int value, Currency currency) {
        Account account = getAccountByCurrency(currency);
        if (account != null) {
            account.withdrawnMoney(value);
            return "Take your " + value + " " + currency +
                    ". Remaining in your account: " + account.getMoney_amount();
        } else {
            return "You doesn't have " + currency + " account";
        }
    }

    String displayDepositByCurrency(Currency currency, LocalDate term) {
        Deposit deposit = getDepositByCurrency(currency);
        if (deposit != null) {
            return "Your " + currency + " deposit have " + deposit.getMoney_amount() + " " + currency
                    + " for year with " + deposit.getYear_percent() + " %. " + "In a year you will have "
                    + deposit.displayInfoByTerm(currency, term) + " USD";
        } else {
            return "You doesn't have " + currency + " account";
        }
    }

    String displayAccountByCurrency(Currency currency) {
        Account account = getAccountByCurrency(currency);
        if (account != null) {
            return "Your account with " + currency + " have "
                    + account.displayInfo(currency) + " " + currency;
        } else {
            return "You doesn't have " + currency + " account";
        }
    }

    String displayStorageByCurrency(Currency currency) {
        Storage storage = getStorageByCurrency(currency);
        if (storage != null) {
            return "Your storage with " + currency + " have " + storage.getMass_in_kg()
                    + " Kg and it is " + storage.displayInfo(currency) + " USD";
        } else {
            return "You doesn't have " + currency + " storage";
        }
    }

    private double moneyConverterInUsd(double value, Currency currency) {
        double moneyInUsd = value;
        switch (currency) {
            case EUR:
                moneyInUsd = value * 1.12f;
                return moneyInUsd;
            case RUB:
                moneyInUsd = value * 0.016f;
                return moneyInUsd;
            case UAH:
                moneyInUsd = value * 0.04f;
                return moneyInUsd;
            case BITCOIN:
                moneyInUsd = value * 8245;
                return moneyInUsd;
            case GOLD:
                moneyInUsd = value * 47812;
                return moneyInUsd;
            case SILVER:
                moneyInUsd = value * 561;
                return moneyInUsd;
            default:
                return moneyInUsd;
        }
    }

    String displayAllInfo() {
        double totalAmountOfMoneyOnAccounts = 0;
        double totalAmountOfMoneyOnDeposits = 0;
        double totalAmountOfMoneyInStorages = 0;

        for (Account account : accounts) {
            totalAmountOfMoneyOnAccounts += moneyConverterInUsd(account.getMoney_amount(), account.getCurrency());
        }
        for (Deposit deposit : deposits) {
            totalAmountOfMoneyOnDeposits += moneyConverterInUsd(deposit.getMoney_amount(), deposit.getCurrency());
        }
        for (Storage storage : storages) {
            totalAmountOfMoneyInStorages += storage.displayInfo(storage.getType_of_metal());
        }
        total_amount = totalAmountOfMoneyOnDeposits + totalAmountOfMoneyOnAccounts + totalAmountOfMoneyInStorages;
        return "Client " + name + " " + age + " years with average income " + average_income + " USD" + "\n" +
        displayInfoAboutDeposits() + "\n" +
        displayInfoAboutAccounts() + "\n" +
        displayInfoAboutStorages() + "\n" +
        "Your total money amount in bank = " + total_amount + " USD" + "\n" +
        "----------------";
    }

    private String displayInfoAboutStorages() {
        if (!storages.isEmpty()) {
            for (Storage storage : storages) {
                return "Your storage with " + storage.getType_of_metal() + " have "
                        + storage.getMass_in_kg() + " Kg and it is "
                        + storage.displayInfo(storage.getType_of_metal()) + " USD";
            }
        }
        return "You haven't storages";
    }

    private String displayInfoAboutAccounts() {
        if (!accounts.isEmpty()) {
            for (Account account : accounts) {
                return "Your " + account.getCurrency() + " account have "
                        + account.displayInfo(account.getCurrency()) + " " + account.getCurrency();
            }
        }
        return "You haven't accounts";
    }

    private String displayInfoAboutDeposits() {
        if (!deposits.isEmpty()) {
            for (Deposit deposit : deposits) {
                return "Your " + deposit.getCurrency() + " deposit have " + deposit.getMoney_amount() + " "
                        + deposit.getCurrency() + " for year with " + deposit.getYear_percent() + " %. "
                        + "In a year you will have " + deposit.displayInfo(deposit.getCurrency()) + " USD";
            }
        }
        return "You haven't deposits";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return age == client.age &&
                Double.compare(client.average_income, average_income) == 0 &&
                Double.compare(client.total_amount, total_amount) == 0 &&
                name.equals(client.name) &&
                Objects.equals(deposits, client.deposits) &&
                Objects.equals(accounts, client.accounts) &&
                Objects.equals(storages, client.storages);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, average_income, deposits, accounts, storages, total_amount);
    }
}