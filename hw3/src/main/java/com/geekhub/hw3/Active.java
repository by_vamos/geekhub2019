package com.geekhub.hw3;

public interface Active {
    void addMoney(double value);

    double displayInfo(Currency currency);
}
