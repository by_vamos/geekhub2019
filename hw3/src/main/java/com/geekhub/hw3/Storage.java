package com.geekhub.hw3;

import java.util.Objects;

public class Storage implements Active {
    private Currency type_of_metal;
    private double mass_in_kg;

    Currency getType_of_metal() {
        return type_of_metal;
    }

    double getMass_in_kg() {
        return mass_in_kg;
    }

    Storage(double mass_in_kg, Currency type_of_metal) {
        this.mass_in_kg = mass_in_kg;
        this.type_of_metal = type_of_metal;
    }

    @Override
    public void addMoney(double mass_in_kg) {
        this.mass_in_kg += mass_in_kg;
    }

    @Override
    public double displayInfo(Currency currency) {
        if (type_of_metal.equals(Currency.GOLD)) {
            return mass_in_kg * 47812;
        } else {
            return mass_in_kg * 561;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Storage storage = (Storage) o;
        return mass_in_kg == storage.mass_in_kg &&
                type_of_metal == storage.type_of_metal;
    }

    @Override
    public int hashCode() {
        return Objects.hash(type_of_metal, mass_in_kg);
    }
}
