package com.geekhub.hw3;

public enum Currency {
    USD,
    EUR,
    UAH,
    RUB,
    GOLD,
    SILVER,
    BITCOIN
}