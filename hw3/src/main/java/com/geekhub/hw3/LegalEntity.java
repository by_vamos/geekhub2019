package com.geekhub.hw3;

import java.util.Objects;

class LegalEntity extends Client {
    private int postcode;
    private String formOfOwnership;

    LegalEntity(String name, int age, int average_income, int postcode, String formOfOwnership) {
        super(name, age, average_income);
        this.postcode = postcode;
        this.formOfOwnership = formOfOwnership;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        LegalEntity that = (LegalEntity) o;
        return postcode == that.postcode &&
                formOfOwnership.equals(that.formOfOwnership);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), postcode, formOfOwnership);
    }
}
