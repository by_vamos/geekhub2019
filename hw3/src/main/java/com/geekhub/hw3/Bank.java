package com.geekhub.hw3;

import java.util.ArrayList;

class Bank {

    static String getTotalBankAmount(ArrayList<Client> clients) {
        double totalBankAmount = 0;
        if (!clients.isEmpty()) {
            for (Client client : clients) {
                totalBankAmount += client.getTotal_amount();
            }
        }
        return "Whole price of actives in the bank = " + totalBankAmount;
    }

}
