package com.geekhub.hw3;

import java.util.ArrayList;

public class BankPlatform {
    public static void main(String[] args) {
        ArrayList<Client> clients = new ArrayList<>();

        Client max = new Client("Max", 23, 2500);
        clients.add(max);
        max.getDeposits().add(new Deposit(2000, Currency.USD, 7));
        max.getAccounts().add(new Account(400, Currency.USD));
        max.getAccounts().add(new Account(200, Currency.EUR));
        max.getStorages().add(new Storage(2, Currency.SILVER));
        LegalEntity amazon = new LegalEntity("Amazon", 25, 93000000,
                18000, "Private company");
        clients.add(amazon);
        amazon.getAccounts().add(new Account(1500000, Currency.USD));
        amazon.getStorages().add(new Storage(20, Currency.GOLD));

        System.out.println(max.addMoneyOnAccountByCurrency(300, Currency.USD));
        System.out.println(max.displayAccountByCurrency(Currency.USD));
        System.out.println(max.addMoneyOnAccountByCurrency(600, Currency.BITCOIN));
        System.out.println(max.displayStorageByCurrency(Currency.SILVER));
        System.out.println(amazon.displayStorageByCurrency(Currency.GOLD));
        System.out.println(max.displayAllInfo());
        System.out.println(amazon.displayAllInfo());
        System.out.println(Bank.getTotalBankAmount(clients));
    }

}
