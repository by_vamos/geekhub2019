package com.geekhub.hw3;

import java.time.LocalDate;
import java.util.Objects;

public class Deposit implements Active {
    private final LocalDate START_DATE = LocalDate.now();
    private final LocalDate HALF_YEAR = START_DATE.plusMonths(6);
    private final LocalDate FULL_YEAR = START_DATE.plusYears(1);
    private Currency currency;
    private double money_amount;
    private double year_percent;

    Deposit(double money_amount, Currency currency, int year_percent) {
        this.money_amount = money_amount;
        this.currency = currency;
        this.year_percent = year_percent;
    }

    Currency getCurrency() {
        return currency;
    }

    double getMoney_amount() {
        return money_amount;
    }

    double getYear_percent() {
        return year_percent;
    }

    public LocalDate getStartDate() {
        return START_DATE;
    }

    public LocalDate getHalfYear() {
        return HALF_YEAR;
    }

    public LocalDate getFullYear() {
        return FULL_YEAR;
    }

    @Override
    public void addMoney(double value) {
        money_amount += value;
    }


    @Override
    public double displayInfo(Currency currency) {
        return money_amount + (money_amount * year_percent) / 100;
    }

    double displayInfoByTerm(Currency currency, LocalDate term) {
        if (term.equals(FULL_YEAR)) {
            return money_amount + (money_amount * year_percent) / 100;
        }
        return money_amount + ((money_amount * year_percent) / 100 / 2);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Deposit deposit = (Deposit) o;
        return Double.compare(deposit.money_amount, money_amount) == 0 &&
                Double.compare(deposit.year_percent, year_percent) == 0 &&
                currency == deposit.currency;
    }

    @Override
    public int hashCode() {
        return Objects.hash(currency, money_amount, year_percent);
    }
}
