package com.geekhub.hw3;

import java.util.Objects;

public class Account implements Active {
    private double money_amount;
    private Currency currency;

    Currency getCurrency() {
        return currency;
    }

    double getMoney_amount() {
        return money_amount;
    }

    Account(double money_amount, Currency currency) {
        this.currency = currency;
        this.money_amount = money_amount;
    }

    @Override
    public void addMoney(double value) {
        money_amount += value;
    }

    @Override
    public double displayInfo(Currency currency) {
        return money_amount;
    }

    void withdrawnMoney(double value) {
        money_amount -= value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return money_amount == account.money_amount &&
                currency == account.currency;
    }

    @Override
    public int hashCode() {
        return Objects.hash(money_amount, currency);
    }
}
