package com.geekhub.hw5.task4;

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;

import static org.junit.Assert.*;

public class TaskManagerTest {

    private HashSet<String> homework;
    private HashSet<String> animals;
    private HashSet<String> shop;
    private HashSet<String> pet;
    private List<Task> tasks;

    @Before
    public void setUp() {
        homework = new HashSet<>();
        homework.add("homework");
        animals = new HashSet<>();
        animals.add("animals");
        shop = new HashSet<>();
        shop.add("shop");
        pet = new HashSet<>();
        pet.add("shop");
        pet.add("animals");
        tasks = new ArrayList<>();
        tasks.add(new Task(1, TaskType.IMPORTANT, "Buy bread", false, shop,
                LocalDate.of(2019, Month.NOVEMBER, 30)));
        tasks.add(new Task(2, TaskType.COMMON, "Clean the house", true, homework, LocalDate.now()));
        tasks.add(new Task(3, TaskType.URGENT, "Rearrange the furniture", false, homework,
                LocalDate.now()));
        tasks.add(new Task(4, TaskType.IMPORTANT, "Feed the cat", false, animals,
                LocalDate.of(2019, Month.NOVEMBER, 24)));
        tasks.add(new Task(5, TaskType.COMMON, "Buy cheese", true, shop,
                LocalDate.of(2019, Month.SEPTEMBER, 28)));
        tasks.add(new Task(6, TaskType.IMPORTANT, "To learn a poem", true, homework, LocalDate.now()));
        tasks.add(new Task(7, TaskType.URGENT, "Buy pet toy", true, pet, LocalDate.now()));
    }

    @Test
    public void find5NearestImportantTasks() {
        List<Task> importantTasks = TaskManager.find5NearestImportantTasks(tasks);
        List<Task> resTasks = new ArrayList<>();
        resTasks.add(new Task(4, TaskType.IMPORTANT, "Feed the cat", false, animals,
                LocalDate.of(2019, Month.NOVEMBER, 24)));
        resTasks.add(new Task(1, TaskType.IMPORTANT, "Buy bread", false, shop,
                LocalDate.of(2019, Month.NOVEMBER, 30)));

        assertEquals(importantTasks, resTasks);
    }

    @Test
    public void getUniqueCategories() {
        List<String> uniqueCategories = TaskManager.getUniqueCategories(tasks);
        List<String> res = Arrays.asList("shop", "homework", "animals");

        assertEquals(uniqueCategories, res);
    }

    @Test
    public void getCategoriesWithTasks() {
        Map<String, List<Task>> categoriesWithTasks = TaskManager.getCategoriesWithTasks(tasks);
        Map<String, List<Task>> resMap = new HashMap<>();

        List<Task> shopTasks = new ArrayList<>();
        List<Task> homeworkTasks = new ArrayList<>();
        List<Task> animalsTasks = new ArrayList<>();
        List<Task> shopAnimalsTasks = new ArrayList<>();

        shopTasks.add(new Task(1, TaskType.IMPORTANT, "Buy bread", false, shop,
                LocalDate.of(2019, Month.NOVEMBER, 30)));
        shopTasks.add(new Task(5, TaskType.COMMON, "Buy cheese", true, shop,
                LocalDate.of(2019, Month.SEPTEMBER, 28)));
        homeworkTasks.add(new Task(2, TaskType.COMMON, "Clean the house", true, homework, LocalDate.now()));
        homeworkTasks.add(new Task(3, TaskType.URGENT, "Rearrange the furniture", false, homework,
                LocalDate.now()));
        homeworkTasks.add(new Task(6, TaskType.IMPORTANT, "To learn a poem", true, homework, LocalDate.now()));
        animalsTasks.add(new Task(4, TaskType.IMPORTANT, "Feed the cat", false, animals,
                LocalDate.of(2019, Month.NOVEMBER, 24)));
        shopAnimalsTasks.add(new Task(7, TaskType.URGENT, "Buy pet toy", true, pet, LocalDate.now()));

        resMap.put("shop", shopTasks);
        resMap.put("homework", homeworkTasks);
        resMap.put("animals", animalsTasks);
        resMap.put("shopanimals", shopAnimalsTasks);

        assertEquals(categoriesWithTasks, resMap);
    }

    @Test
    public void splitTasksIntoDoneAndInProgress() {
        Map<Boolean, List<Task>> booleanListMap = TaskManager.splitTasksIntoDoneAndInProgress(tasks);
        Map<Boolean, List<Task>> resMap = new HashMap<>();
        List<Task> falseTasks = new ArrayList<>();
        List<Task> trueTasks = new ArrayList<>();

        falseTasks.add(new Task(1, TaskType.IMPORTANT, "Buy bread", false, shop,
                LocalDate.of(2019, Month.NOVEMBER, 30)));
        falseTasks.add(new Task(3, TaskType.URGENT, "Rearrange the furniture", false, homework,
                LocalDate.now()));
        falseTasks.add(new Task(4, TaskType.IMPORTANT, "Feed the cat", false, animals,
                LocalDate.of(2019, Month.NOVEMBER, 24)));

        trueTasks.add(new Task(2, TaskType.COMMON, "Clean the house", true, homework, LocalDate.now()));
        trueTasks.add(new Task(5, TaskType.COMMON, "Buy cheese", true, shop,
                LocalDate.of(2019, Month.SEPTEMBER, 28)));
        trueTasks.add(new Task(6, TaskType.IMPORTANT, "To learn a poem", true, homework, LocalDate.now()));
        trueTasks.add(new Task(7, TaskType.URGENT, "Buy pet toy", true, pet, LocalDate.now()));

        resMap.put(false, falseTasks);
        resMap.put(true, trueTasks);

        assertEquals(booleanListMap, resMap);
    }

    @Test
    public void existsTaskOfCategory() {
        boolean magazine = TaskManager.existsTaskOfCategory(tasks, "magazine");
        boolean shop = TaskManager.existsTaskOfCategory(tasks, "shop");

        assertFalse(magazine);
        assertTrue(shop);
    }

    @Test
    public void getTitlesOfTasks() {
        String titlesOfTasks = TaskManager.getTitlesOfTasks(tasks, 5, 7);
        String res = "Buy cheese, To learn a poem, Buy pet toy";

        assertEquals(titlesOfTasks, res);
    }

    @Test
    public void getCountsByCategories() {
        Map<String, Long> countsByCategories = TaskManager.getCountsByCategories(tasks);
        Map<String, Long> resMap = new HashMap<>();
        resMap.put("shop", 2L);
        resMap.put("homework", 3L);
        resMap.put("animals", 1L);
        resMap.put("shopanimals", 1L);

        assertEquals(countsByCategories, resMap);
    }

    @Test
    public void findTaskWithBiggestCountOfCategories() {
        Task task = TaskManager.findTaskWithBiggestCountOfCategories(tasks);
        Task res = new Task(7, TaskType.URGENT, "Buy pet toy", true, pet, LocalDate.now());

        assertEquals(task, res);
    }
}