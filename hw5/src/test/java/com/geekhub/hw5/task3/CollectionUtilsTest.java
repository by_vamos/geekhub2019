package com.geekhub.hw5.task3;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class CollectionUtilsTest {

    private List<Integer> numbers = Arrays.asList(1, 26, 3, 4, 54, 6, 6, 7, 6, 81, 9, 10);
    private List<String> words = Arrays.asList("apple", "banana", "apricot", "banana", "orange");


    @Test
    public void filter() {
        List<Integer> filter = CollectionUtils.filter(numbers, n -> n > 9);
        List<Integer> res = Arrays.asList(26, 54, 81, 10);

        assertEquals(filter, res);
    }

    @Test
    public void anyMatch() {
        boolean match = CollectionUtils.anyMatch(numbers, n -> n > 9);

        assertTrue(match);
    }

    @Test
    public void allMatch() {
        boolean match = CollectionUtils.allMatch(numbers, n -> n > 0);

        assertTrue(match);
    }

    @Test
    public void noneMatch() {
        boolean match = CollectionUtils.noneMatch(numbers, n -> n > 10);

        assertFalse(match);
    }

    @Test
    public void map() {
        List<String> map = CollectionUtils.map(numbers, (number) -> {
            if (number > 11)
                return number + " higher than 11";
            return number + " less than 11";
        }, ArrayList::new);
        List<String> res = Arrays.asList("1 less than 11", "26 higher than 11", "3 less than 11", "4 less than 11",
                "54 higher than 11", "6 less than 11", "6 less than 11", "7 less than 11", "6 less than 11",
                "81 higher than 11", "9 less than 11", "10 less than 11");

        assertEquals(map, res);
    }

    @Test
    public void max() {
        Optional<Integer> max = CollectionUtils.max(numbers, (n, n2) -> {
            if (n.compareTo(n2) > 0) {
                return 1;
            } else if (n.compareTo(n2) < 0) {
                return -1;
            }
            return 0;
        });
        Optional<Integer> res = Optional.of(81);

        assertEquals(max, res);
    }

    @Test
    public void min() {
        Optional<Integer> min = CollectionUtils.min(numbers, (n, n2) -> {
            if (n.compareTo(n2) > 0) {
                return 1;
            } else if (n.compareTo(n2) < 0) {
                return -1;
            }
            return 0;
        });
        Optional<Integer> res = Optional.of(1);

        assertEquals(min, res);
    }

    @Test
    public void distinct() {
        List<Integer> numsList = CollectionUtils.distinct(numbers, ArrayList::new);
        List<String> wordsList = CollectionUtils.distinct(words, ArrayList::new);
        List<Integer> numsRes = Arrays.asList(1, 26, 3, 4, 54, 6, 7, 81, 9, 10);
        List<String> wordsRes = Arrays.asList("apple", "banana", "apricot", "orange");

        assertEquals(numsList, numsRes);
        assertEquals(wordsList, wordsRes);
    }

    @Test
    public void forEach() {
    }

    @Test
    public void reduce() {
        Optional<Integer> reduce = CollectionUtils.reduce(numbers, Integer::sum);
        Optional<Integer> res = Optional.of(213);

        assertEquals(reduce, res);
    }

    @Test
    public void reduceWithSeed() {
        Integer reduce = CollectionUtils.reduce(87, numbers, Integer::sum);
        Integer res = 300;

        assertEquals(reduce, res);
    }

    @Test
    public void partitionBy() {
        Map<Boolean, List<Integer>> booleanListMap = CollectionUtils.partitionBy(numbers, (Integer number) -> number > 11,
                HashMap::new, ArrayList::new);
        Map<Boolean, List<Integer>> resMap = new HashMap<>();
        resMap.put(true, Arrays.asList(26, 54, 81));
        resMap.put(false, Arrays.asList(1, 3, 4, 6, 6, 7, 6, 9, 10));

        assertEquals(booleanListMap, resMap);
    }

    @Test
    public void groupBy() {
        Map<String, List<Integer>> stringListMap = CollectionUtils.groupBy(numbers, (number) -> {
                    if (number < 11) {
                        return "numbers less than 11: ";
                    } else if (number < 50) {
                        return "number higher than 10 and less than 50: ";
                    } else {
                        return "number higher than 50: ";
                    }
                },
                HashMap::new, ArrayList::new);
        Map<String, List<Integer>> resMap = new HashMap<>();
        resMap.put("number higher than 10 and less than 50: ", Arrays.asList(26));
        resMap.put("numbers less than 11: ", Arrays.asList(1, 3, 4, 6, 6, 7, 6, 9, 10));
        resMap.put("number higher than 50: ", Arrays.asList(54, 81));

        assertEquals(stringListMap, resMap);
    }

    @Test
    public void toMap() {
        Map<Character, String> characterStringMap = CollectionUtils.toMap(words, (word) -> word.charAt(0),
                (word) -> word, (word1, word2) -> word1 + " & " + word2, HashMap::new);
        Map<Character, String> resMap = new HashMap<>();
        resMap.put('a', "apple & apricot");
        resMap.put('b', "banana & banana");
        resMap.put('o', "orange");

        assertEquals(characterStringMap, resMap);
    }

    @Test
    public void partitionByAndMapElement() {
        Map<Boolean, List<Float>> booleanListMap = CollectionUtils.partitionByAndMapElement(numbers,
                (Integer number) -> number > 11,
                HashMap::new, ArrayList::new,
                (Integer number) -> number.floatValue() + 0.5F
        );
        Map<Boolean, List<Float>> resMap = new HashMap<>();
        resMap.put(false, Arrays.asList(1.5f, 3.5f, 4.5f, 6.5f, 6.5f, 7.5f, 6.5f, 9.5f, 10.5f));
        resMap.put(true, Arrays.asList(26.5f, 54.5f, 81.5f));

        assertEquals(booleanListMap, resMap);
    }

    @Test
    public void groupByAndMapElement() {
        Map<String, List<Integer>> stringListMap = CollectionUtils.groupByAndMapElement(numbers, (number) -> {
                    if (number < 11) {
                        return "numbers less than 11: ";
                    } else if (number < 50) {
                        return "number higher than 10 and less than 50: ";
                    } else {
                        return "number higher than 50: ";
                    }
                },
                HashMap::new, ArrayList::new,
                (number) -> number - 3);
        Map<String, List<Integer>> resMap = new HashMap<>();
        resMap.put("number higher than 10 and less than 50: ", Arrays.asList(23));
        resMap.put("numbers less than 11: ", Arrays.asList(-2, 0, 1, 3, 3, 4, 3, 6, 7));
        resMap.put("number higher than 50: ", Arrays.asList(51, 78));

        assertEquals(stringListMap, resMap);
    }
}