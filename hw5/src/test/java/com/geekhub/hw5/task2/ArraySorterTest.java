package com.geekhub.hw5.task2;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class ArraySorterTest {

    private User mike = new User("Mike", 21);
    private User stan = new User("Stan", 26);
    private User stanly = new User("Stan", 24);
    private User leonardo = new User("Leonardo", 19);
    private User alex = new User("Alex", 23);
    private User billy = new User("Billy", 21);
    private User[] users = new User[]{mike, stan, stanly, leonardo, alex, billy};

    @Test
    public void bubbleSort() {
        String stringAsc = Arrays.toString(ArraySorter.bubbleSort(users, Direction.ASC));
        String resAsc = "[User{name='Alex', age='23'}, User{name='Billy', age='21'}, User{name='Leonardo', age='19'}," +
                " User{name='Mike', age='21'}, User{name='Stan', age='24'}, User{name='Stan', age='26'}]";
        String stringDesc = Arrays.toString(ArraySorter.bubbleSort(users, Direction.DESC));
        String resDesc = "[User{name='Stan', age='26'}, User{name='Stan', age='24'}, User{name='Mike', age='21'}," +
                " User{name='Leonardo', age='19'}, User{name='Billy', age='21'}, User{name='Alex', age='23'}]";
        assertEquals(stringAsc, resAsc);
        assertEquals(stringDesc, resDesc);
    }

    @Test
    public void insertionSort() {
        String stringAsc = Arrays.toString(ArraySorter.insertionSort(users, Direction.ASC));
        String resAsc = "[User{name='Alex', age='23'}, User{name='Billy', age='21'}, User{name='Leonardo', age='19'}," +
                " User{name='Mike', age='21'}, User{name='Stan', age='24'}, User{name='Stan', age='26'}]";
        String stringDesc = Arrays.toString(ArraySorter.insertionSort(users, Direction.DESC));
        String resDesc = "[User{name='Stan', age='26'}, User{name='Stan', age='24'}, User{name='Mike', age='21'}," +
                " User{name='Leonardo', age='19'}, User{name='Billy', age='21'}, User{name='Alex', age='23'}]";
        assertEquals(stringAsc, resAsc);
        assertEquals(stringDesc, resDesc);
    }

    @Test
    public void selectionSort() {
        String stringAsc = Arrays.toString(ArraySorter.selectionSort(users, Direction.ASC));
        String resAsc = "[User{name='Alex', age='23'}, User{name='Billy', age='21'}, User{name='Leonardo', age='19'}," +
                " User{name='Mike', age='21'}, User{name='Stan', age='24'}, User{name='Stan', age='26'}]";
        String stringDesc = Arrays.toString(ArraySorter.selectionSort(users, Direction.DESC));
        String resDesc = "[User{name='Stan', age='26'}, User{name='Stan', age='24'}, User{name='Mike', age='21'}," +
                " User{name='Leonardo', age='19'}, User{name='Billy', age='21'}, User{name='Alex', age='23'}]";
        assertEquals(stringAsc, resAsc);
        assertEquals(stringDesc, resDesc);
    }
}