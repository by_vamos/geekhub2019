package com.geekhub.hw5.task3;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

class CollectionUtils {

    private CollectionUtils() {
    }

    static <E> List<E> generate(Supplier<E> generator,
                                Supplier<List<E>> listFactory,
                                int count) {
        List<E> generatedList = listFactory.get();
        for (int i = 0; i < count; i++) {
            E gen = generator.get();
            generatedList.add(gen);
        }
        return generatedList;
    }

    static <E> List<E> filter(List<E> elements, Predicate<E> filter) {
        List<E> filtered = new ArrayList<>();
        for (E element : elements) {
            if (filter.test(element)) {
                filtered.add(element);
            }
        }
        return filtered;
    }

    static <E> boolean anyMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if (predicate.test(element)) {
                return true;
            }
        }
        return false;
    }

    static <E> boolean allMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if (!predicate.test(element)) {
                return false;
            }
        }
        return true;
    }

    static <E> boolean noneMatch(List<E> elements, Predicate<E> predicate) {
        for (E element : elements) {
            if (predicate.test(element))
                return false;
        }
        return true;
    }

    static <T, R> List<R> map(List<T> elements,
                              Function<T, R> mappingFunction,
                              Supplier<List<R>> listFactory) {
        List<R> generatedList = listFactory.get();
        R result;
        for (T element : elements) {
            result = mappingFunction.apply(element);
            generatedList.add(result);
        }
        return generatedList;
    }

    static <E> Optional<E> max(List<E> elements, Comparator<E> comparator) {
        Iterator<E> iterator = elements.iterator();
        E max = iterator.next();
        for (E element : elements) {
            if (!max.equals(element)) {
                int res = comparator.compare(element, max);
                if (res > 0)
                    max = element;
            }
        }
        return Optional.of(max);
    }

    static <E> Optional<E> min(List<E> elements, Comparator<E> comparator) {
        Iterator<E> iterator = elements.iterator();
        E min = iterator.next();
        for (E element : elements) {
            if (!min.equals(element)) {
                int res = comparator.compare(element, min);
                if (res < 0)
                    min = element;
            }
        }
        return Optional.of(min);
    }

    static <E> List<E> distinct(List<E> elements, Supplier<List<E>> listFactory) {
        List<E> insideList = listFactory.get();
        int count = 0;
        for (E element : elements) {
            for (E insideListElement : insideList) {
                if (insideListElement.equals(element)) {
                    count++;
                    break;
                }
            }
            if (count == 0)
                insideList.add(element);
            count = 0;
        }
        return insideList;
    }

    static <E> void forEach(List<E> elements, Consumer<E> consumer) {
        for (E element : elements) {
            consumer.accept(element);
        }
    }

    static <E> Optional<E> reduce(List<E> elements, BinaryOperator<E> accumulator) {
        Iterator<E> iterator = elements.iterator();
        E result = iterator.next();
        for (int i = 1; i < elements.size(); i++) {
            result = accumulator.apply(result, iterator.next());
        }
        return Optional.of(result);
    }

    static <E> E reduce(E seed, List<E> elements, BinaryOperator<E> accumulator) {
        E result = seed;
        for (E element : elements) {
            result = accumulator.apply(result, element);
        }
        return result;
    }

    static <E> Map<Boolean, List<E>> partitionBy(List<E> elements,
                                                 Predicate<E> predicate,
                                                 Supplier<Map<Boolean, List<E>>> mapFactory,
                                                 Supplier<List<E>> listFactory) {
        Map<Boolean, List<E>> insideMap = mapFactory.get();
        List<E> insideListTrue = listFactory.get();
        List<E> insideListFalse = listFactory.get();
        for (E element : elements) {
            if (predicate.test(element)) {
                insideListTrue.add(element);
            } else {
                insideListFalse.add(element);
            }
        }
        insideMap.put(true, insideListTrue);
        insideMap.put(false, insideListFalse);
        return insideMap;
    }

    static <T, K> Map<K, List<T>> groupBy(List<T> elements,
                                          Function<T, K> classifier,
                                          Supplier<Map<K, List<T>>> mapFactory,
                                          Supplier<List<T>> listFactory) {
        Map<K, List<T>> insideMap = mapFactory.get();
        K resClassifier;
        for (T element : elements) {
            resClassifier = classifier.apply(element);
            if (!insideMap.containsKey(resClassifier)) {
                List<T> insideList = listFactory.get();
                insideMap.put(resClassifier, insideList);
                insideList.add(element);
            } else {
                List<T> insideList = insideMap.get(resClassifier);
                insideList.add(element);
                insideMap.put(resClassifier, insideList);
            }
        }
        return insideMap;
    }

    static <T, K, U> Map<K, U> toMap(List<T> elements,
                                     Function<T, K> keyFunction,
                                     Function<T, U> valueFunction,
                                     BinaryOperator<U> mergeFunction,
                                     Supplier<Map<K, U>> mapFactory) {
        Map<K, U> insideMap = mapFactory.get();
        for (T element : elements) {
            K resClassifier = keyFunction.apply(element);
            U resValue = valueFunction.apply(element);
            if (!insideMap.containsKey(resClassifier)) {
                insideMap.put(resClassifier, resValue);
            } else {
                U resFunc = mergeFunction.apply(insideMap.get(resClassifier), resValue);
                insideMap.put(resClassifier, resFunc);
            }
        }
        return insideMap;
    }

    //ADDITIONAL METHODS

    static <E, T> Map<Boolean, List<T>> partitionByAndMapElement(List<E> elements,
                                                                 Predicate<E> predicate,
                                                                 Supplier<Map<Boolean, List<T>>> mapFactory,
                                                                 Supplier<List<T>> listFactory,
                                                                 Function<E, T> elementMapper) {
        Map<Boolean, List<T>> insideMap = mapFactory.get();
        List<T> insideListTrue = listFactory.get();
        List<T> insideListFalse = listFactory.get();
        for (E element : elements) {
            T result = elementMapper.apply(element);
            if (predicate.test(element)) {
                insideListTrue.add(result);
            } else {
                insideListFalse.add(result);
            }
        }
        insideMap.put(true, insideListTrue);
        insideMap.put(false, insideListFalse);
        return insideMap;
    }

    static <T, U, K> Map<K, List<U>> groupByAndMapElement(List<T> elements,
                                                          Function<T, K> classifier,
                                                          Supplier<Map<K, List<U>>> mapFactory,
                                                          Supplier<List<U>> listFactory,
                                                          Function<T, U> elementMapper) {
        Map<K, List<U>> insideMap = mapFactory.get();
        K resClassifier;
        for (T element : elements) {
            resClassifier = classifier.apply(element);
            U result = elementMapper.apply(element);
            if (!insideMap.containsKey(resClassifier)) {
                List<U> insideList = listFactory.get();
                insideMap.put(resClassifier, insideList);
                insideList.add(result);
            } else {
                List<U> insideList = insideMap.get(resClassifier);
                insideList.add(result);
                insideMap.put(resClassifier, insideList);
            }
        }
        return insideMap;
    }
}