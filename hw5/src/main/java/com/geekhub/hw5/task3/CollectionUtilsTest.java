package com.geekhub.hw5.task3;

import java.util.*;

public class CollectionUtilsTest {
    public static void main(String[] args) {
        List<Integer> numbers = Arrays.asList(1, 26, 3, 4, 54, 6, 6, 7, 6, 81, 9, 10);
        List<String> words = Arrays.asList("apple", "banana", "apricot", "banana", "orange");

        System.out.println(numbers);
        System.out.println("allMatch: " + CollectionUtils.allMatch(numbers, n -> n > 0));
        System.out.println("anyMatch: " + CollectionUtils.anyMatch(numbers, n -> n > 9));
        System.out.println("noneMatch: " + CollectionUtils.noneMatch(numbers, n -> n > 10));
        System.out.println("filter: " + CollectionUtils.filter(numbers, n -> n > 9));
        System.out.println("max: " + CollectionUtils.max(numbers, (n, n2) -> {
            if (n.compareTo(n2) > 0) {
                return 1;
            } else if (n.compareTo(n2) < 0) {
                return -1;
            }
            return 0;
        }));
        System.out.println("min: " + CollectionUtils.min(numbers, (n, n2) -> {
            if (n.compareTo(n2) > 0) {
                return 1;
            } else if (n.compareTo(n2) < 0) {
                return -1;
            }
            return 0;
        }));
        System.out.println("distinct: " + CollectionUtils.distinct(numbers, ArrayList::new));
        System.out.println("distinct: " + CollectionUtils.distinct(words, ArrayList::new));
        System.out.println("generate: " + CollectionUtils.generate(Math::random, ArrayList::new, 5));
        System.out.print("forEach: ");
        CollectionUtils.forEach(numbers, System.out::print);
        System.out.println();
        System.out.println("reduce: " + CollectionUtils.reduce(numbers, Integer::sum));
        System.out.println("reduce with seed: " + CollectionUtils.reduce(87, numbers, Integer::sum));
        System.out.println("map: " + CollectionUtils.map(numbers, (number) -> {
            if (number > 11)
                return number + " higher than 11";
            return number + " less than 11";
        }, ArrayList::new));
        System.out.println("partitionBy: " + CollectionUtils.partitionBy(numbers, (Integer number) -> number > 11,
                HashMap::new, ArrayList::new));
        System.out.println("groupBy: " + CollectionUtils.groupBy(numbers, (number) -> {
                    if (number < 11) {
                        return "numbers less than 11: ";
                    } else if (number < 50) {
                        return "number higher than 10 and less than 50: ";
                    } else {
                        return "number higher than 50: ";
                    }
                },
                HashMap::new, ArrayList::new));
        System.out.println("toMap: " + CollectionUtils.toMap(words, (word) -> word.charAt(0),
                (word) -> word, (word1, word2) -> word1 + " & " + word2, HashMap::new));
        System.out.println("partitionByAndMapElement: " + CollectionUtils.partitionByAndMapElement(numbers,
                (Integer number) -> number > 11,
                HashMap::new, ArrayList::new,
                (Integer number) -> number.floatValue() + 0.5F
        ));
        System.out.println("groupByAndMapElement: " + CollectionUtils.groupByAndMapElement(numbers, (number) -> {
                    if (number < 11) {
                        return "numbers less than 11: ";
                    } else if (number < 50) {
                        return "number higher than 10 and less than 50: ";
                    } else {
                        return "number higher than 50: ";
                    }
                },
                HashMap::new, ArrayList::new,
                (number) -> number - 3));
    }
}
