package com.geekhub.hw5.task1;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        Inventory inventory = new Inventory();
        Scanner sc = new Scanner(System.in);

        System.out.println("Choose a product (milk, bread, cheese) or type done when done");
        boolean done = false;
        while (!done) {
            String product = sc.nextLine();
            switch (product) {
                case "milk":
                    inventory.addProduct(inventory.getMilk());
                    break;
                case "bread":
                    inventory.addProduct(inventory.getBread());
                    break;
                case "cheese":
                    inventory.addProduct(inventory.getCheese());
                    break;
                case "done":
                    done = true;
                    break;
                default:
                    break;
            }
        }
        System.out.println("Total price of products in inventory " + inventory.totalPrice() + " UAH");
    }
}
