package com.geekhub.hw5.task1;

import java.util.ArrayList;

class Inventory {

    private Product milk = new Product("Milk", 11.07d, 1);
    private Product bread = new Product("Bread", 19.43d, 1);
    private Product cheese = new Product("Cheese", 32.82d, 1);
    private ArrayList<Product> products = new ArrayList<>();

    Product getMilk() {
        return milk;
    }

    Product getBread() {
        return bread;
    }

    Product getCheese() {
        return cheese;
    }

    void addProduct(Product product) {
        products.add(product);
    }

    private double totalPriceProduct(ArrayList<Product> list) {
        return list.stream()
                .mapToDouble(product -> product.getPrice() * product.getQuantityOnHand())
                .sum();
    }

    double totalPrice() {
        return totalPriceProduct(products);
    }
}
