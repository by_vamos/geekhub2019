package com.geekhub.hw5.task1;

import java.util.Objects;

public class Product {
    private String name;
    private double price;
    private int quantityOnHand;

    Product(String name, double price, int quantityOnHand) {
        this.name = name;
        this.price = price;
        this.quantityOnHand = quantityOnHand;
    }

    public String getName() {
        return name;
    }

    double getPrice() {
        return price;
    }

    int getQuantityOnHand() {
        return quantityOnHand;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Double.compare(product.price, price) == 0 &&
                quantityOnHand == product.quantityOnHand &&
                name.equals(product.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, price, quantityOnHand);
    }
}
