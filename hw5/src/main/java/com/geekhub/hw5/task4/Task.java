package com.geekhub.hw5.task4;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

public class Task {
    private int id;
    private TaskType type;
    private String title;
    private boolean done;
    private Set<String> categories;
    private LocalDate startsOn;

    Task(int id, TaskType type, String title, boolean done, Set<String> categories, LocalDate startsOn) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.done = done;
        this.categories = categories;
        this.startsOn = startsOn;
    }

    int getId() {
        return id;
    }

    TaskType getType() {
        return type;
    }

    String getTitle() {
        return title;
    }

    boolean isDone() {
        return done;
    }

    Set<String> getCategories() {
        return categories;
    }

    LocalDate getStartsOn() {
        return startsOn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id == task.id &&
                done == task.done &&
                type == task.type &&
                title.equals(task.title) &&
                categories.equals(task.categories) &&
                startsOn.equals(task.startsOn);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, title, done, categories, startsOn);
    }

    @Override
    public String toString() {
        return title;
    }
}
