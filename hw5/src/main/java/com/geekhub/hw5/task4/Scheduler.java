package com.geekhub.hw5.task4;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static com.geekhub.hw5.task4.TaskManager.*;

public class Scheduler {
    public static void main(String[] args) {
        HashSet<String> homework = new HashSet<>();
        homework.add("homework");
        HashSet<String> animals = new HashSet<>();
        animals.add("animals");
        HashSet<String> shop = new HashSet<>();
        shop.add("shop");
        HashSet<String> pet = new HashSet<>();
        pet.add("shop");
        pet.add("animals");
        List<Task> tasks = new ArrayList<>();
        tasks.add(new Task(1, TaskType.IMPORTANT, "Buy bread", false, shop,
                LocalDate.of(2019, Month.NOVEMBER, 30)));
        tasks.add(new Task(2, TaskType.COMMON, "Clean the house", true, homework, LocalDate.now()));
        tasks.add(new Task(3, TaskType.URGENT, "Rearrange the furniture", false, homework,
                LocalDate.now()));
        tasks.add(new Task(4, TaskType.IMPORTANT, "Feed the cat", false, animals,
                LocalDate.of(2019, Month.NOVEMBER, 24)));
        tasks.add(new Task(5, TaskType.COMMON, "Buy cheese", true, shop,
                LocalDate.of(2019, Month.SEPTEMBER, 28)));
        tasks.add(new Task(6, TaskType.IMPORTANT, "To learn a poem", true, homework, LocalDate.now()));
        tasks.add(new Task(7, TaskType.URGENT, "Buy pet toy", true, pet, LocalDate.now()));

        System.out.println("Split tasks into done and in progress: " + splitTasksIntoDoneAndInProgress(tasks));
        System.out.println("Find 5 undone nearest important tasks: " + find5NearestImportantTasks(tasks));
        System.out.println("Get unique categories: " + getUniqueCategories(tasks));
        System.out.println("Get categories with tasks: " + getCategoriesWithTasks(tasks));
        System.out.println("Exists task of category magazine: " + existsTaskOfCategory(tasks, "magazine"));
        System.out.println("Exists task of category shop: " + existsTaskOfCategory(tasks, "shop"));
        System.out.println("Get titles of tasks 1-4: " + getTitlesOfTasks(tasks, 1, 4));
        System.out.println("Get titles of tasks 5-7: " + getTitlesOfTasks(tasks, 5, 7));
        System.out.println("Get categories names length statistics: " + getCategoriesNamesLengthStatistics(tasks));
        System.out.println("Find task with biggest count of categories: " + findTaskWithBiggestCountOfCategories(tasks));
        System.out.println("Get counts by categories: " + getCountsByCategories(tasks));
    }
}
