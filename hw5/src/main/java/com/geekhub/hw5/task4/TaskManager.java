package com.geekhub.hw5.task4;

import java.util.*;
import java.util.stream.Collectors;

class TaskManager {
    static List<Task> find5NearestImportantTasks(List<Task> tasks) {
        return tasks.stream()
                .filter(task -> task.getType().equals(TaskType.IMPORTANT))
                .filter(task -> !task.isDone())
                .sorted(Comparator.comparing(Task::getStartsOn))
                .limit(5)
                .collect(Collectors.toList());
    }

    static List<String> getUniqueCategories(List<Task> tasks) {
        return tasks.stream()
                .map(task -> {
                    Set<String> categories = task.getCategories();
                    String value = "";
                    for (String category : categories) {
                        value = category;
                    }
                    return value;
                })
                .distinct()
                .collect(Collectors.toList());
    }

    static Map<String, List<Task>> getCategoriesWithTasks(List<Task> tasks) {
        return tasks.stream()
                .collect(Collectors.groupingBy(task -> getCategoryStringFormat(task.getCategories())));
    }

    static Map<Boolean, List<Task>> splitTasksIntoDoneAndInProgress(List<Task> tasks) {
        return tasks.stream()
                .collect(Collectors.groupingBy(Task::isDone));
    }

    static boolean existsTaskOfCategory(List<Task> tasks, String category) {
        return tasks.stream()
                .anyMatch(task -> getCategoryStringFormat(task.getCategories()).equals(category));
    }

    static String getTitlesOfTasks(List<Task> tasks, int startNo, int endNo) {
        return tasks.stream()
                .filter(task -> task.getId() >= startNo && task.getId() <= endNo)
                .map(Task::getTitle)
                .collect(Collectors.joining(", "));
    }


    static Map<String, Long> getCountsByCategories(List<Task> tasks) {
        return tasks.stream()
                .collect(Collectors.toMap(
                        task -> getCategoryStringFormat(task.getCategories()),
                        task -> {
                            long count = 0;
                            count++;
                            return count;
                        },
                        Long::sum
                ));
    }

    static IntSummaryStatistics getCategoriesNamesLengthStatistics(List<Task> tasks) {
        return tasks.stream()
                .mapToInt(task -> getCategoryStringFormat(task.getCategories()).length())
                .summaryStatistics();
    }

    static Task findTaskWithBiggestCountOfCategories(List<Task> tasks) {
        return tasks.stream()
                .max(Comparator.comparingInt(task -> task.getCategories().size()))
                .orElse(tasks.get(0));
    }

    private static String getCategoryStringFormat(Set<String> categories) {
        String value;
        StringBuilder sb = new StringBuilder();
        for (String category : categories) {
            sb.append(category);
        }
        value = sb.toString();
        return value;
    }
}