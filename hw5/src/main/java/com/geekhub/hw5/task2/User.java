package com.geekhub.hw5.task2;

public class User implements Comparable<User> {

    private final String name;
    private final int age;

    User(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public int compareTo(User user) {
        int nameResult = this.name.compareTo(user.name);
        int ageResult = this.age - user.age;
        if (nameResult == 0) {
            if (ageResult <= 0) {
                return -1;
            } else {
                return 1;
            }
        }
        if (nameResult < 0) {
            return -1;
        } else {
            return 1;
        }
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                '}';
    }
}
