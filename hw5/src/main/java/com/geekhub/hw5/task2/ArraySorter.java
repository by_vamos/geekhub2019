package com.geekhub.hw5.task2;

class ArraySorter {

    static <T extends Comparable<T>> T[] bubbleSort(T[] array, Direction direction) {
        T[] insideArray = array.clone();
        for (int i = insideArray.length - 1; i >= 1; i--) {
            for (int j = 0; j < i; j++) {
                int result = insideArray[j].compareTo(insideArray[j + 1]) * direction.getDirection();
                    if (result > 0) {
                        swap(insideArray, j);
                    }
            }
        }
        return insideArray;
    }

    static <T extends Comparable<T>> T[] insertionSort(T[] array, Direction direction) {
        T[] insideArray = array.clone();
        for (int i = 1; i < insideArray.length; i++) {
            for (int j = i; j > 0; j--) {
                int result = insideArray[j].compareTo(insideArray[j - 1]) * direction.getDirection();
                    if (result < 0) {
                        swap(insideArray, j - 1);
                    }
            }
        }
        return insideArray;
    }

    static <T extends Comparable<T>> T[] selectionSort(T[] array, Direction direction) {
        T[] insideArray = array.clone();
        for (int i = 0; i < insideArray.length; i++) {
            int min = i;
            for (int j = i + 1; j < insideArray.length; j++) {
                int result = insideArray[min].compareTo(insideArray[j]) * direction.getDirection();
                    if (result > 0) {
                        min = j;
                    }
            }
            T temp = insideArray[i];
            insideArray[i] = insideArray[min];
            insideArray[min] = temp;
        }
        return insideArray;
    }

    private static <T extends Comparable<T>> void swap(T[] insideArray, int j) {
        T temp = insideArray[j];
        insideArray[j] = insideArray[j + 1];
        insideArray[j + 1] = temp;
    }
}
