<%@ page import="com.geekhub.hw10.task3.FeedbackDatabase" %>
<%@ page import="com.geekhub.hw10.task2.Feedback" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Feedback with JSP</title>
</head>
<body>
<form action="/guest-book?page=1" method="post">
    <textarea name="message" placeholder="Message"></textarea><br/>
    <select name="rating">
        <option value="rating" selected disabled hidden>Choose rating</option>
        <option value="1">1</option>
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        </select>
    <input type="submit">
</form>
<a href="/logout">Logout</a><br/>
<nav aria-label="Page navigation">
    <ul class="pagination">
        <li class="page-item"><a class="page-link" href="#">Previous</a></li>
        <li class="page-item"><a class="page-link" href="/guest-book?page=1">1</a></li>
        <li class="page-item"><a class="page-link" href="/guest-book?page=2">2</a></li>
        <li class="page-item"><a class="page-link" href="/guest-book?page=3">3</a></li>
        <li class="page-item"><a class="page-link" href="#">Next</a></li>
    </ul>
</nav>
<c:forEach var="feedback" items="${resultList}">
    <c:out value = "${feedback}"/><br/>
</c:forEach>
<%
    FeedbackDatabase.clear();
%>
</body>
</html>
