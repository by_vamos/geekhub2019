package com.geekhub.hw10.task3;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static java.util.Objects.nonNull;

@WebFilter("/guest-book")
public class LoginFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        final HttpServletRequest req = (HttpServletRequest) request;
        final HttpServletResponse resp = (HttpServletResponse) response;
        final HttpSession session = req.getSession();
        final String sessionName = (String) req.getSession().getAttribute("name");
        final String sessionPassword = (String) req.getSession().getAttribute("password");

        if (nonNull(session) && nonNull(sessionName) && nonNull(sessionPassword)) {
            if (LoginDatabase.verify(sessionName + sessionPassword)) {
                session.setAttribute("name", sessionName);
                session.setAttribute("password", sessionPassword);
                session.setAttribute("page", req.getParameter("page"));
                chain.doFilter(req, resp);
            }
        } else {
            resp.sendRedirect("/login");
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {
    }

    @Override
    public void destroy() {
    }
}
