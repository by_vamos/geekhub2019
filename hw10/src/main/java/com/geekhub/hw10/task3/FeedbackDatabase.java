package com.geekhub.hw10.task3;

import com.geekhub.hw10.task2.Feedback;
import java.util.ArrayList;
import java.util.List;

public class FeedbackDatabase {

    private static List<Feedback> db = new ArrayList<>();

    public static void init() {

        for (int i = 0; i < 20 ; i++) {
            Feedback feedback = new Feedback();
            feedback.setName("Name" + i);
            feedback.setMessage("Message" + i);
            feedback.setRating("Rating" + i);
            feedback.setDate("Date" + i);
            db.add(feedback);
        }
    }

    public static List<Feedback> getFeedbackDb() {
        return db;
    }

    public static void clear() {
        db.clear();
    }
}
