package com.geekhub.hw10.task3;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static java.util.Objects.nonNull;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/loginJsp.jsp")
                .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession();
        String sessionName = req.getParameter("name");
        String sessionPassword = req.getParameter("password");

        if (nonNull(session) && nonNull(sessionName) && nonNull(sessionPassword)) {
            if (LoginDatabase.verify(sessionName + sessionPassword)) {
                session.setAttribute("name", sessionName);
                session.setAttribute("password", sessionPassword);
                req.getRequestDispatcher("/guest-book").forward(req, resp);
            }
        } else {
            req.getRequestDispatcher("/guest-book").forward(req, resp);
        }
    }
}