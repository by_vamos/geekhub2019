package com.geekhub.hw10.task3;

import java.util.Arrays;
import java.util.List;

public class LoginDatabase {

    private static List<String> database = Arrays.asList("Joe1", "Sam2", "Din3", "Alex4");

    public static boolean verify(String data) {
        for (String s : database) {
            if(s.equals(data))
                return true;
        }
        return false;
    }
}
