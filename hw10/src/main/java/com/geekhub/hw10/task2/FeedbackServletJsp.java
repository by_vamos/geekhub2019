package com.geekhub.hw10.task2;

import com.geekhub.hw10.task3.FeedbackDatabase;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FeedbackServletJsp extends HttpServlet {

    private List<Feedback> resultList = new CopyOnWriteArrayList<>();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        Feedback feedback = new Feedback();
        feedback.setName((String) req.getSession().getAttribute("name"));
        feedback.setMessage(req.getParameter("message"));
        feedback.setRating(req.getParameter("rating"));
        feedback.setDate(LocalTime.now().format(DateTimeFormatter.ofPattern("hh:mm:ss")));
        int page = Integer.parseInt(req.getParameter("page"));

        resultList.add(0, feedback);
        FeedbackDatabase.init();
        resultList.addAll(FeedbackDatabase.getFeedbackDb());
        req.getSession().setAttribute("countPage", resultList.size() / 5);
        List<Feedback> list = resultList.stream().skip((page - 1) * 5).limit(5).collect(Collectors.toList());
        req.getSession().setAttribute("resultList", list);
        resp.sendRedirect("/guest-book");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.getRequestDispatcher("indexJsp.jsp")
                .forward(request, response);
    }
}
