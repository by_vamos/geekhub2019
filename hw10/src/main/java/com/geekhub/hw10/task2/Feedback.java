package com.geekhub.hw10.task2;

public class Feedback {

    private String name;
    private String message;
    private String rating;
    private String date;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "date='" + date + '\'' +
                ", name='" + name + '\'' +
                ", message='" + message + '\'' +
                ", rating='" + rating + '\'' +
                '}';
    }
}
