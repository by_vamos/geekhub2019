package com.geekhub.hw10.task1;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class FeedbackServlet extends HttpServlet {

    private StringBuilder resultString = new StringBuilder();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String name = req.getParameter("name");
        String message = req.getParameter("message");
        String rating = req.getParameter("rating");
        String time = LocalTime.now().format(DateTimeFormatter.ofPattern("hh:mm:ss"));


        if (!(name == null && message == null && rating == null)) {
            String result = time + " Name: " + name + " Message: " + message + " Rating: " + rating + "<br/>";
            resultString.insert(0, result);
        }
        resp.sendRedirect("/main");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        try (PrintWriter writer = response.getWriter()) {
            writer.println("<html lang=\"en\">\n" +
                    "<head>\n" +
                    "    <meta charset=\"UTF-8\">\n" +
                    "    <title>Feedback</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<form action=\"/main\" method=\"post\">\n" +
                    "    <input name=\"name\" placeholder=\"Name\"/><br/>\n" +
                    "    <textarea name=\"message\" placeholder=\"Message\"></textarea><br/>\n" +
                    "    <select name=\"rating\">\n" +
                    "        <option value=\"rating\" selected disabled hidden>Choose rating</option>\n" +
                    "        <option value=\"1\">1</option>\n" +
                    "        <option value=\"2\">2</option>\n" +
                    "        <option value=\"3\">3</option>\n" +
                    "        <option value=\"4\">4</option>\n" +
                    "        <option value=\"5\">5</option>\n" +
                    "    </select>\n" +
                    "    <input type=\"submit\">\n" +
                    "</form>\n" +
                    "<p name=\"result\">" +
                    resultString.toString() +
                    "</p>\n" +
                    "</body>\n" +
                    "</html>\n");
        }
    }
}
