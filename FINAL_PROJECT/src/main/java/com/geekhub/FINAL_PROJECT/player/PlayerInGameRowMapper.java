package com.geekhub.FINAL_PROJECT.player;

import com.geekhub.FINAL_PROJECT.game.Role;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PlayerInGameRowMapper implements RowMapper<PlayerInGame> {

    @Override
    public PlayerInGame mapRow(ResultSet rs, int rowNum) throws SQLException {
        PlayerInGame player = new PlayerInGame();
        player.setName(rs.getString("player_name"));
        player.setPlayer_place(rs.getInt("player_place"));
        player.setRole(Role.valueOf(rs.getString("role")));
        player.setAdditional_points(rs.getDouble("additional_points"));
        player.setLx_points(rs.getDouble("lx_points"));
        return player;
    }
}
