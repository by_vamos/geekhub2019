package com.geekhub.FINAL_PROJECT.player;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PlayerService {

    private PlayerRepository playerRepository;

    @Autowired
    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public void savePlayer(String name) {
        List<String> allPlayerNames = getAllPlayerNames();
        boolean match = false;
        for (String playerName : allPlayerNames) {
            if (playerName.equals(name)) {
                match = true;
                break;
            }
        }
        if (!match) {
            Player player = new Player();
            player.setName(name);
            playerRepository.savePlayer(player);
        }
    }

    public List<Player> getAllPlayers() {
        return playerRepository.getAllPlayers();
    }

    public List<String> getAllPlayerNames() {
        return playerRepository.getAllPlayerNames();
    }

    public Player getPlayerById(int playerId) {
        return playerRepository.getPlayerById(playerId);
    }

    public Player getPlayerByName(String name) {
        return playerRepository.getPlayerByName(name);
    }

    public List<PlayerInGame> getPlayersInGameByGameId(int games_id) {
        return playerRepository.getPlayersInGameByGameId(games_id);
    }
}
