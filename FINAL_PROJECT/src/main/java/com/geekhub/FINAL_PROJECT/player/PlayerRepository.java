package com.geekhub.FINAL_PROJECT.player;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public class PlayerRepository {

    private NamedParameterJdbcTemplate jdbcTemplate;
    private PlayerRowMapper playerRowMapper = new PlayerRowMapper();
    private PlayerInGameRowMapper playerInGameRowMapper = new PlayerInGameRowMapper();

    @Autowired
    public PlayerRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public Player savePlayer(Player player) {
        String sql = "INSERT INTO players (name, create_date) VALUES (:name, :date) RETURNING id";
        MapSqlParameterSource params = new MapSqlParameterSource();
        String date = LocalDate.now().getYear() + " " + LocalDate.now().getMonth() +
                " " + LocalDate.now().getDayOfMonth();
        params.addValue("name", player.getName());
        params.addValue("date", date);
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, params, keyHolder);
        player.setId(keyHolder.getKey().intValue());
        return player;
    }

    public List<Player> getAllPlayers() {
        String sql = "SELECT id, name, create_date FROM players ORDER BY id";
        return jdbcTemplate.query(sql, playerRowMapper);
    }

    public List<String> getAllPlayerNames() {
        String sql = "SELECT name FROM players ORDER BY id";
        return jdbcTemplate.query(sql, (rs, rowNum) -> rs.getString("name"));
    }

    public Player getPlayerById(int playerId) {
        String sql = "SELECT id, name, create_date FROM players WHERE id = :id ORDER BY id";
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue("id", playerId);
        return jdbcTemplate.queryForObject(sql, param, playerRowMapper);
    }

    public Player getPlayerByName(String playerName) {
        String sql = "SELECT id, name, create_date FROM players WHERE name = :name ORDER BY id";
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue("name", playerName);
        return jdbcTemplate.queryForObject(sql, param, playerRowMapper);
    }

    public List<PlayerInGame> getPlayersInGameByGameId (int game_id) {
        String sql = "SELECT player_place, player_name, role, additional_points, lx_points " +
                "FROM players_in_game WHERE game_id = :game_id";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("game_id", game_id);
        return jdbcTemplate.query(sql, params, playerInGameRowMapper);
    }
}
