package com.geekhub.FINAL_PROJECT.player;

import org.springframework.stereotype.Component;

@Component
public class PlayerDtoConverter {

    public PlayerDto convert(Player player) {
        PlayerDto playerDto = new PlayerDto();
        playerDto.setId(player.getId());
        playerDto.setName(player.getName());
        playerDto.setCreate_date(player.getCreate_date());
        return playerDto;
    }
}
