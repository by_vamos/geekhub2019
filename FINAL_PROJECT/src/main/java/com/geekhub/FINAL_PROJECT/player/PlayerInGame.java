package com.geekhub.FINAL_PROJECT.player;

import com.geekhub.FINAL_PROJECT.game.Role;

public class PlayerInGame extends Player {

    private int player_place;
    private Role role;
    private int penalty;
    private double additional_points;
    private double lx_points;

    public int getPlayer_place() {
        return player_place;
    }

    public void setPlayer_place(int player_place) {
        this.player_place = player_place;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public int getPenalty() {
        return penalty;
    }

    public void setPenalty(int penalty) {
        this.penalty = penalty;
    }

    public double getAdditional_points() {
        return additional_points;
    }

    public void setAdditional_points(double additional_points) {
        this.additional_points = additional_points;
    }

    public double getLx_points() {
        return lx_points;
    }

    public void setLx_points(double lx_points) {
        this.lx_points = lx_points;
    }

    @Override
    public String toString() {
        return "name=" + getName() +
                ", place=" + player_place +
                ", role=" + role +
                ", add_points=" + additional_points +
                ", lx_points=" + lx_points;
    }
}
