package com.geekhub.FINAL_PROJECT.player;

import com.geekhub.FINAL_PROJECT.db.DataLoader;
import com.geekhub.FINAL_PROJECT.download.ExportService;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/players")
public class PlayerController {

    private PlayerService playerService;
    private PlayerDtoConverter playerDtoConverter;
    private ExportService exportService;

    @Autowired
    public PlayerController(PlayerService playerService, PlayerDtoConverter playerDtoConverter, ExportService exportService) {
        this.playerService = playerService;
        this.playerDtoConverter = playerDtoConverter;
        this.exportService = exportService;
    }

    @GetMapping
    public String getIndexPage(ModelMap model) {
        List<Player> allPlayers = playerService.getAllPlayers();
        List<PlayerDto> playersDtoList = new ArrayList<>();
        for (Player player : allPlayers) {
            PlayerDto playerDto = playerDtoConverter.convert(player);
            playersDtoList.add(playerDto);
        }
        model.addAttribute("players", playersDtoList);
        return "players/index";
    }

    @PostMapping
    public String newPlayerForm(@RequestParam String name) {
        playerService.savePlayer(name);
        return "redirect:/players";
    }

    @GetMapping("{playerName}/show")
    public String getShowPage(@PathVariable String playerName, ModelMap model) {
        Player playerByName = playerService.getPlayerByName(playerName);
        PlayerDto playerDto = playerDtoConverter.convert(playerByName);
        model.addAttribute("player", playerDto);
        return "players/show";
    }

    @GetMapping("/doc")
    public ResponseEntity<byte[]> downloadDoc() {
        String players = getPlayersInStringFormat();
        return exportService.downloadDoc(players, "players");
    }

    @GetMapping("/pdf")
    public void downloadPdf(HttpServletResponse response) throws DocumentException, IOException {
        String players = getPlayersInStringFormat();
        String filename = "players.pdf";
        byte[] pdf = exportService.preparePdf(players, new Document());
        exportService.downloadFile(response, pdf, filename, MediaType.APPLICATION_PDF);
    }

    @GetMapping("/excel")
    public void downloadExcel(HttpServletResponse response) throws IOException {
        List<Player> allPlayers = playerService.getAllPlayers();
        String filename = "players.xls";
        MediaType mediaType = MediaType.valueOf("application/vnd.ms-excel");
        byte[] preparedExcel = exportService.prepareExcel(allPlayers);
        exportService.downloadFile(response, preparedExcel, filename, mediaType);
    }

    private String getPlayersInStringFormat() {
        StringBuilder players = new StringBuilder();
        List<Player> allPlayers = playerService.getAllPlayers();
        for (Player player : allPlayers) {
            players.append(player.toString());
        }
        return players.toString();
    }
}
