package com.geekhub.FINAL_PROJECT.db;

import com.geekhub.FINAL_PROJECT.player.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class DataLoader {

    private PlayerService playerService;

    @Autowired
    public DataLoader(PlayerService playerService) throws IOException {
        this.playerService = playerService;
        loadDataInPlayersTable();
    }

    public void loadDataInPlayersTable() throws IOException {
        List<String> players = readDataFromFile(new File("./FINAL_PROJECT/src/main/resources/static/players.txt"));
        List<String> allPlayerNames = playerService.getAllPlayerNames();
        for (String player : players) {
            boolean match = false;
            for (String playerName : allPlayerNames) {
                if (playerName.equals(player)) {
                    match = true;
                    break;
                }
            }
            if (!match) {
                playerService.savePlayer(player);
            }
        }
    }

    private List<String> readDataFromFile(File source) throws IOException {
        List<String> list = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(source))) {
            String s;
            while ((s = reader.readLine()) != null)
                list.add(s);
            return list;
        }
    }
}
