package com.geekhub.FINAL_PROJECT.rating;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RatingService {

    private RatingRepository ratingRepository;

    @Autowired
    public RatingService(RatingRepository ratingRepository) {
        this.ratingRepository = ratingRepository;
    }

    public List<PlayerStats> getPlayersStats(){
        return ratingRepository.getPlayersStats();
    }
}
