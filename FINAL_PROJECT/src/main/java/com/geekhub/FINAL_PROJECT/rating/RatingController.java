package com.geekhub.FINAL_PROJECT.rating;

import com.geekhub.FINAL_PROJECT.download.ExportService;
import com.geekhub.FINAL_PROJECT.player.Player;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/rating")
public class RatingController {

    private RatingService ratingService;
    private PlayerStatsDtoConverter playerStatsDtoConverter;
    private ExportService exportService;

    @Autowired
    public RatingController(RatingService ratingService, PlayerStatsDtoConverter playerStatsDtoConverter, ExportService exportService) {
        this.ratingService = ratingService;
        this.playerStatsDtoConverter = playerStatsDtoConverter;
        this.exportService = exportService;
    }

    @GetMapping
    public String getIndexPage(ModelMap model){
        List<PlayerStats> playersStats = ratingService.getPlayersStats();
        List<PlayerStatsDto> playerStatsDtos = new ArrayList<>();
        for (PlayerStats playerStats : playersStats) {
            playerStatsDtos.add(playerStatsDtoConverter.convert(playerStats));
        }
        model.addAttribute("playersStats", playerStatsDtos);
        return "rating/index";
    }

    @GetMapping("/doc")
    public ResponseEntity<byte[]> downloadDoc() {
        String rating = getRatingInStringFormat();
        return exportService.downloadDoc(rating, "rating");
    }

    @GetMapping("/pdf")
    public void downloadPdf(HttpServletResponse response) throws DocumentException, IOException {
        String rating = getRatingInStringFormat();
        String filename = "rating.pdf";
        byte[] pdf = exportService.preparePdf(rating, new Document());
        exportService.downloadFile(response, pdf, filename, MediaType.APPLICATION_PDF);
    }

    @GetMapping("/excel")
    public void downloadExcel(HttpServletResponse response) throws IOException {
        List<PlayerStats> playerStats = ratingService.getPlayersStats();
        String filename = "rating.xls";
        MediaType mediaType = MediaType.valueOf("application/vnd.ms-excel");
        byte[] preparedExcel = exportService.prepareExcel(playerStats);
        exportService.downloadFile(response, preparedExcel, filename, mediaType);
    }

    private String getRatingInStringFormat() {
        StringBuilder stats = new StringBuilder();
        List<PlayerStats> playersStats = ratingService.getPlayersStats();
        for (PlayerStats playersStat : playersStats) {
            stats.append(playersStat.toString());
        }
        return stats.toString();
    }
}
