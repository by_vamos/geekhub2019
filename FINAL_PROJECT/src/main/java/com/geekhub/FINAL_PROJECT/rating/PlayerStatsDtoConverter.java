package com.geekhub.FINAL_PROJECT.rating;

import org.springframework.stereotype.Component;

@Component
public class PlayerStatsDtoConverter {

    public PlayerStatsDto convert(PlayerStats playerStats) {
        PlayerStatsDto playerStatsDto = new PlayerStatsDto();
        playerStatsDto.setName(playerStats.getName());
        playerStatsDto.setKpd(playerStats.getKpd());
        playerStatsDto.setTotalPoints(playerStats.getTotalPoints());
        playerStatsDto.setPointsPerGame(playerStats.getPointsPerGame());
        playerStatsDto.setAddPoints(playerStats.getAddPoints());
        playerStatsDto.setLxPoints(playerStats.getLxPoints());
        playerStatsDto.setGames(playerStats.getGames());
        playerStatsDto.setRatedGames(playerStats.getRatedGames());
        playerStatsDto.setWinRate(playerStats.getWinRate());
        return playerStatsDto;
    }
}
