package com.geekhub.FINAL_PROJECT.rating;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class RatingRepository {

    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    public RatingRepository(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<PlayerStats> getPlayersStats() {
        List<PlayerStats> playersStats = new ArrayList<>();
        List<String> allPlayersInRating = getAllPlayersNameInRating();
        for (String player_name : allPlayersInRating) {
            double addPoints = sumAddPointsByName(player_name);
            double lxPoints = sumLxPointsByName(player_name);
            int winRatedGames = countWinRatedGamesByName(player_name);
            int games = countAllGamesByName(player_name);
            int ratedGames = countRatedGamesByName(player_name);
            double totalPoints = winRatedGames * 2 + addPoints + lxPoints;
            double winRate = (double) winRatedGames * 100 / ratedGames;
            double pointsPerGame = totalPoints / ratedGames;
            double kpd = (pointsPerGame * ratedGames * winRate / 100 * 0.004) * 100;

            PlayerStats stats = new PlayerStats();
            stats.setName(player_name);
            stats.setGames(games);
            stats.setRatedGames(ratedGames);
            stats.setAddPoints(addPoints);
            stats.setLxPoints(lxPoints);
            stats.setTotalPoints(totalPoints);
            stats.setWinRate(winRate);
            stats.setPointsPerGame(pointsPerGame);
            stats.setKpd(kpd);
            playersStats.add(stats);
        }
        return playersStats.stream().sorted((player1, player2) -> {
            double kpd1 = player1.getKpd();
            double kpd2 = player2.getKpd();
            return Double.compare(kpd2, kpd1);
        }).collect(Collectors.toList());
    }

    private List<String> getAllPlayersNameInRating() {
        String sql = "SELECT player_name\n" +
                "FROM players_in_game\n" +
                "GROUP BY player_name";
        return jdbcTemplate.query(sql, (rs, rowNum) -> rs.getString("player_name"));
    }

    private Integer countAllGamesByName(String player_name) {
        String sql = "SELECT count(player_name) as games\n" +
                "FROM players_in_game\n" +
                "WHERE player_name = :player_name";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("player_name", player_name);
        return jdbcTemplate.queryForObject(sql, params, (rs, rowNum) -> rs.getInt("games"));
    }

    private Integer countRatedGamesByName(String player_name) {
        String sql = "SELECT count(player_name) as rated_games\n" +
                "FROM players_in_game\n" +
                "         JOIN games g ON players_in_game.game_id = g.id\n" +
                "WHERE g.rating_game = true\n" +
                "  AND player_name = :player_name";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("player_name", player_name);
        return jdbcTemplate.queryForObject(sql, params, (rs, rowNum) -> rs.getInt("rated_games"));
    }

    private Double sumAddPointsByName(String player_name) {
        String sql = "SELECT sum(additional_points) " +
                "FROM players_in_game " +
                "JOIN games g ON players_in_game.game_id = g.id " +
                "WHERE g.rating_game = true " +
                "AND player_name = :player_name";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("player_name", player_name);
        return jdbcTemplate.queryForObject(sql, params, (rs, rowNum) -> rs.getDouble("sum"));
    }

    private Double sumLxPointsByName(String player_name) {
        String sql = "SELECT sum(lx_points) as lx_points\n" +
                "FROM players_in_game\n" +
                "         JOIN games g ON players_in_game.game_id = g.id\n" +
                "WHERE g.rating_game = true\n" +
                "  AND player_name = :player_name";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("player_name", player_name);
        return jdbcTemplate.queryForObject(sql, params, (rs, rowNum) -> rs.getDouble("lx_points"));
    }

    private Integer countWinRatedGamesByName(String player_name) {
        String sql = "SELECT sub.win_rated_games\n" +
                "FROM (SELECT player_name,\n" +
                "             count(g.id) as win_rated_games\n" +
                "      FROM players_in_game\n" +
                "               JOIN games g on players_in_game.game_id = g.id\n" +
                "      WHERE g.rating_game = true\n" +
                "          AND (g.winner = 'МАФИЯ' AND (role = 'ДОН' OR role = 'МАФИЯ'))\n" +
                "         OR (g.winner = 'ГОРОД' AND (role = 'ШЕРИФ' OR role = 'МИРНЫЙ'))\n" +
                "      GROUP BY player_name) as sub\n" +
                "WHERE player_name = :player_name";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("player_name", player_name);
        try {
            return jdbcTemplate.queryForObject(sql, params, (rs, rowNum) -> rs.getInt("win_rated_games"));
        } catch (EmptyResultDataAccessException e) {
            return 0;
        }
    }
}
