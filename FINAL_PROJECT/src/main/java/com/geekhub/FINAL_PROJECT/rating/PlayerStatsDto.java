package com.geekhub.FINAL_PROJECT.rating;

public class PlayerStatsDto {

    private String name;
    private double kpd;
    private double totalPoints;
    private double pointsPerGame;
    private double addPoints;
    private double lxPoints;
    private double winRate;
    private int ratedGames;
    private int games;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getKpd() {
        return kpd;
    }

    public void setKpd(double kpd) {
        this.kpd = kpd;
    }

    public double getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(double totalPoints) {
        this.totalPoints = totalPoints;
    }

    public double getPointsPerGame() {
        return pointsPerGame;
    }

    public void setPointsPerGame(double pointsPerGame) {
        this.pointsPerGame = pointsPerGame;
    }

    public double getAddPoints() {
        return addPoints;
    }

    public void setAddPoints(double addPoints) {
        this.addPoints = addPoints;
    }

    public double getLxPoints() {
        return lxPoints;
    }

    public void setLxPoints(double lxPoints) {
        this.lxPoints = lxPoints;
    }

    public double getWinRate() {
        return winRate;
    }

    public void setWinRate(double winRate) {
        this.winRate = winRate;
    }

    public int getRatedGames() {
        return ratedGames;
    }

    public void setRatedGames(int ratedGames) {
        this.ratedGames = ratedGames;
    }

    public int getGames() {
        return games;
    }

    public void setGames(int games) {
        this.games = games;
    }

    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", kpd=" + kpd +
                ", totalPoints=" + totalPoints +
                ", pointsPerGame=" + pointsPerGame +
                ", addPoints=" + addPoints +
                ", lxPoints=" + lxPoints +
                ", winRate=" + winRate +
                "%, ratedGames=" + ratedGames +
                ", games=" + games +
                " \n";
    }
}

