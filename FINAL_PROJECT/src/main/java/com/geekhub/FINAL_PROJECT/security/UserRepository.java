package com.geekhub.FINAL_PROJECT.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final UserRowMapper userRowMapper;

    @Autowired
    public UserRepository(NamedParameterJdbcTemplate jdbcTemplate, UserRowMapper userRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.userRowMapper = userRowMapper;
    }

    public void save(User user) {
        String sql = "INSERT INTO users (name, password) VALUES (:name, :password)";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", user.getUsername());
        params.addValue("password", user.getPassword());
        jdbcTemplate.update(sql, params);
    }

    public User findByUsername(String username) {
        String sql = "SELECT id, name, password FROM users WHERE name = :username";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("username", username);
        return jdbcTemplate.queryForObject(sql, params, userRowMapper);
    }

    public User findById(int userId) {
        String sql = "SELECT id, name, password FROM users WHERE id = :userId";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("userId", userId);
        return jdbcTemplate.queryForObject(sql, params, userRowMapper);
    }

    public List<User> findAll() {
        String sql = "SELECT id, name, password FROM users";
        return jdbcTemplate.query(sql, userRowMapper);
    }
}
