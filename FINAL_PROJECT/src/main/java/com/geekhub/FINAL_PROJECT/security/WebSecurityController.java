package com.geekhub.FINAL_PROJECT.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class WebSecurityController {

    private final UserService userService;

    @Autowired
    public WebSecurityController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/login")
    public String form() {
        return "login";
    }

    @PostMapping("/reg")
    public String addUser(@RequestParam String username,
                          @RequestParam String password,
                          @RequestParam String passconfirm,
                          Model model) {
        User user = new User();
        user.setName(username);
        user.setPassword(password);
        if (!password.equals(passconfirm)) {
            model.addAttribute("passwordError", "Пароли не совпадают");
            return "reg";
        }
        if (!userService.saveUser(user)) {
            model.addAttribute("usernameError", "Пользователь с таким именем уже существует");
            return "reg";
        }
        return "redirect:/";
    }

    @GetMapping("/reg")
    public String reg() {
        return "reg";
    }

}
