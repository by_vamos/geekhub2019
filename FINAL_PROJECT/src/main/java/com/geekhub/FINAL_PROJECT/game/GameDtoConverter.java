package com.geekhub.FINAL_PROJECT.game;

import org.springframework.stereotype.Component;

@Component
public class GameDtoConverter {

    public GameDto convert(Game game) {
        GameDto gameDto = new GameDto();
        gameDto.setId(game.getId());
        gameDto.setWinnerDto(WinnerDto.valueOf(game.getWinner().name()));
        gameDto.setHost(game.getHost());
        gameDto.setDate(game.getDate());
        gameDto.setRating(game.getRating());
        gameDto.setLx(game.getLx());
        gameDto.setPlayers(game.getPlayers());
        return gameDto;
    }

}
