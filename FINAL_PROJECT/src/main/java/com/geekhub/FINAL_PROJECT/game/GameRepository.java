package com.geekhub.FINAL_PROJECT.game;

import com.geekhub.FINAL_PROJECT.player.PlayerInGame;
import com.geekhub.FINAL_PROJECT.player.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class GameRepository {

    private NamedParameterJdbcTemplate jdbcTemplate;
    private GameRowMapper gameRowMapper = new GameRowMapper();
    private PlayerService playerService;

    @Autowired
    public GameRepository(NamedParameterJdbcTemplate jdbcTemplate, PlayerService playerService) {
        this.jdbcTemplate = jdbcTemplate;
        this.playerService = playerService;
    }

    public Game addNewGame(Game game) {
        String sql = "INSERT INTO games(winner, host, date, rating_game)" +
                " VALUES (:winner, :host, :date, :rating_game) RETURNING id";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("winner", game.getWinner().name());
        params.addValue("host", game.getHost());
        params.addValue("date", game.getDate());
        params.addValue("rating_game", game.getRating());
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(sql, params, keyHolder);
        game.setId(keyHolder.getKey().intValue());
        addPlayersInGameById(game);
        addLxInGameById(game);
        return game;
    }

    public Game getGameById(int game_id){
        String sql = "SELECT id, winner, host, date, rating_game FROM games WHERE id = :id";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("id", game_id);
        Game game = jdbcTemplate.queryForObject(sql, params, gameRowMapper);
        assert game != null;
        List<PlayerInGame> playersInGame = playerService.getPlayersInGameByGameId(game.getId());
        game.setPlayers(playersInGame);
        return game;
    }

    public List<Game> getAllGames() {
        String sql = "SELECT id, winner, host, date, rating_game FROM games";
        List<Game> games = jdbcTemplate.query(sql, gameRowMapper);
        for (Game game : games) {
            game.setPlayers(playerService.getPlayersInGameByGameId(game.getId()));
        }
        return games;
    }

    private void addPlayersInGameById(Game game) {
        List<PlayerInGame> players = game.getPlayers();
        Lx lx = game.getLx();
        for (PlayerInGame player : players) {
            if (player.getPlayer_place() == lx.getPy()) {
                player.setLx_points(calculateLxPoints(game));
            } else {
                player.setLx_points(0);
            }
        }
        String sql = "INSERT INTO players_in_game(game_id, player_place, player_name, role, additional_points, lx_points)" +
                "VALUES (:game_id, :player_place, :player_name, :role, :additional_points, :lx_points)";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("game_id", game.getId());
        for (PlayerInGame player : players) {
            params.addValue("player_place", player.getPlayer_place());
            params.addValue("player_name", player.getName());
            params.addValue("role", player.getRole().name());
            params.addValue("additional_points", player.getAdditional_points());
            params.addValue("lx_points", player.getLx_points());
            jdbcTemplate.update(sql, params);
        }
    }

    private void addLxInGameById(Game game) {
        List<PlayerInGame> players = game.getPlayers();
        Lx lx = game.getLx();
        String sql = "INSERT INTO lx(game_id, py, maf1, maf2, maf3) VALUES (:game_id, :py, :maf1, :maf2, :maf3)";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("game_id", game.getId());
        for (PlayerInGame player : players) {
            int place = player.getPlayer_place();
            if (place == lx.getPy()) {
                params.addValue("py", player.getName());
            }
            if (place == lx.getMaf1()) {
                params.addValue("maf1", player.getName());
            }
            if (place == lx.getMaf2()) {
                params.addValue("maf2", player.getName());
            }
            if (place == lx.getMaf3()) {
                params.addValue("maf3", player.getName());
            }
        }
        jdbcTemplate.update(sql, params);
    }

    private double calculateLxPoints(Game game) {
        List<PlayerInGame> players = game.getPlayers();
        Lx lx = game.getLx();
        if (lx.getPy() == 0) {
            return 0;
        }
        PlayerInGame playerInGamePy = null;
        PlayerInGame maf1 = null;
        PlayerInGame maf2 = null;
        PlayerInGame maf3 = null;
        for (PlayerInGame player : players) {
            int place = player.getPlayer_place();
            if (place == lx.getPy()) {
                playerInGamePy = player;
            }
            if (place == lx.getMaf1()) {
                maf1 = player;
            }
            if (place == lx.getMaf2()) {
                maf2 = player;
            }
            if (place == lx.getMaf3()) {
                maf3 = player;
            }
        }
        assert maf1 != null;
        Role role1 = maf1.getRole();
        assert maf2 != null;
        Role role2 = maf2.getRole();
        assert maf3 != null;
        Role role3 = maf3.getRole();

        boolean isMaf1Maf = false;
        boolean isMaf2Maf = false;
        boolean isMaf3Maf = false;

        if (role1.equals(Role.ДОН) | role1.equals(Role.МАФИЯ)) {
            isMaf1Maf = true;
        }
        if (role2.equals(Role.ДОН) | role2.equals(Role.МАФИЯ)) {
            isMaf2Maf = true;
        }
        if (role3.equals(Role.ДОН) | role3.equals(Role.МАФИЯ)) {
            isMaf3Maf = true;
        }

        assert playerInGamePy != null;
        if (isMaf1Maf & isMaf2Maf & isMaf3Maf) {
            return 0.4;
        }

        if (!isMaf1Maf) {
            if (!isMaf2Maf) {
                return 0;
            } else if (!isMaf3Maf) {
                return 0;
            } else {
                return 0.25;
            }
        } else if (isMaf2Maf) {
            return 0.25;
        } else if (isMaf3Maf) {
            return 0.25;
        } else {
            return 0;
        }
    }
}
