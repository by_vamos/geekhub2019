package com.geekhub.FINAL_PROJECT.game;

import com.geekhub.FINAL_PROJECT.player.PlayerInGame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

@Service
public class GameService {

    private GameRepository gameRepository;

    @Autowired
    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public Game addNewGame(WinnerDto winner, String host, Boolean rating, int py, int maf1, int maf2, int maf3,
                           String player1, int penalty1, RoleDto role1, double addPoints1,
                           String player2, int penalty2, RoleDto role2, double addPoints2,
                           String player3, int penalty3, RoleDto role3, double addPoints3,
                           String player4, int penalty4, RoleDto role4, double addPoints4,
                           String player5, int penalty5, RoleDto role5, double addPoints5,
                           String player6, int penalty6, RoleDto role6, double addPoints6,
                           String player7, int penalty7, RoleDto role7, double addPoints7,
                           String player8, int penalty8, RoleDto role8, double addPoints8,
                           String player9, int penalty9, RoleDto role9, double addPoints9,
                           String player10, int penalty10, RoleDto role10, double addPoints10) {
        PlayerInGame playerInGame1 = createPlayer(player1, 1, penalty1, role1, addPoints1);
        PlayerInGame playerInGame2 = createPlayer(player2, 2, penalty2, role2, addPoints2);
        PlayerInGame playerInGame3 = createPlayer(player3, 3, penalty3, role3, addPoints3);
        PlayerInGame playerInGame4 = createPlayer(player4, 4, penalty4, role4, addPoints4);
        PlayerInGame playerInGame5 = createPlayer(player5, 5, penalty5, role5, addPoints5);
        PlayerInGame playerInGame6 = createPlayer(player6, 6, penalty6, role6, addPoints6);
        PlayerInGame playerInGame7 = createPlayer(player7, 7, penalty7, role7, addPoints7);
        PlayerInGame playerInGame8 = createPlayer(player8, 8, penalty8, role8, addPoints8);
        PlayerInGame playerInGame9 = createPlayer(player9, 9, penalty9, role9, addPoints9);
        PlayerInGame playerInGame10 = createPlayer(player10, 10, penalty10, role10, addPoints10);
        List<PlayerInGame> players = Arrays.asList(playerInGame1, playerInGame2, playerInGame3, playerInGame4,
                playerInGame5, playerInGame6, playerInGame7, playerInGame8, playerInGame9, playerInGame10);
        Lx lx = new Lx();
        lx.setPy(py);
        lx.setMaf1(maf1);
        lx.setMaf2(maf2);
        lx.setMaf3(maf3);
        Game game = new Game();
        game.setWinner(Winner.valueOf(winner.name()));
        game.setDate(LocalDate.now().getYear() + " " + LocalDate.now().getMonth() + " "
                + LocalDate.now().getDayOfMonth());
        game.setHost(host);
        game.setRating(rating);
        game.setLx(lx);
        game.setPlayers(players);
        return gameRepository.addNewGame(game);
    }

    public Game getGameById(int game_id) {
        return gameRepository.getGameById(game_id);
    }

    public List<Game> getAllGames() {
        return gameRepository.getAllGames();
    }

    private PlayerInGame createPlayer(String player, int player_place, int penalty, RoleDto role, double addPoints) {
        PlayerInGame playerInGame = new PlayerInGame();
        playerInGame.setName(player);
        playerInGame.setPlayer_place(player_place);
        playerInGame.setPenalty(penalty);
        playerInGame.setRole(Role.valueOf(role.name()));
        playerInGame.setAdditional_points(addPoints);
        return playerInGame;
    }
}
