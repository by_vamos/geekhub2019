package com.geekhub.FINAL_PROJECT.game;

public class Lx {

    private int py;
    private int maf1;
    private int maf2;
    private int maf3;

    public int getPy() {
        return py;
    }

    public void setPy(int py) {
        this.py = py;
    }

    public int getMaf1() {
        return maf1;
    }

    public void setMaf1(int maf1) {
        this.maf1 = maf1;
    }

    public int getMaf2() {
        return maf2;
    }

    public void setMaf2(int maf2) {
        this.maf2 = maf2;
    }

    public int getMaf3() {
        return maf3;
    }

    public void setMaf3(int maf3) {
        this.maf3 = maf3;
    }
}
