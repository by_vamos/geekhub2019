package com.geekhub.FINAL_PROJECT.game;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class GameRowMapper implements RowMapper<Game> {
    @Override
    public Game mapRow(ResultSet rs, int rowNum) throws SQLException {
        Game game = new Game();
        game.setId(rs.getInt("id"));
        game.setWinner(Winner.valueOf(rs.getString("winner")));
        game.setHost(rs.getString("host"));
        game.setDate(rs.getString("date"));
        game.setRating(rs.getBoolean("rating_game"));
        return game;
    }
}
