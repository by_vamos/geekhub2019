package com.geekhub.FINAL_PROJECT.game;

import com.geekhub.FINAL_PROJECT.player.PlayerInGame;

import java.util.List;

public class GameDto {

    private int id;
    private WinnerDto winnerDto;
    private String host;
    private String date;
    private Boolean rating;
    private Lx lx;
    private List<PlayerInGame> players;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public WinnerDto getWinnerDto() {
        return winnerDto;
    }

    public void setWinnerDto(WinnerDto winnerDto) {
        this.winnerDto = winnerDto;
    }

    public Boolean getRating() {
        return rating;
    }

    public void setRating(Boolean rating) {
        this.rating = rating;
    }

    public Lx getLx() {
        return lx;
    }

    public void setLx(Lx lx) {
        this.lx = lx;
    }

    public List<PlayerInGame> getPlayers() {
        return players;
    }

    public void setPlayers(List<PlayerInGame> players) {
        this.players = players;
    }
}
