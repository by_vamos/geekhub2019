package com.geekhub.FINAL_PROJECT.game;

import com.geekhub.FINAL_PROJECT.download.ExportService;
import com.geekhub.FINAL_PROJECT.player.Player;
import com.geekhub.FINAL_PROJECT.player.PlayerService;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/games")
public class GameController {

    private PlayerService playerService;
    private GameService gameService;
    private GameDtoConverter gameDtoConverter;
    private ExportService exportService;

    @Autowired
    public GameController(PlayerService playerService, GameService gameService, GameDtoConverter gameDtoConverter, ExportService exportService) {
        this.playerService = playerService;
        this.gameService = gameService;
        this.gameDtoConverter = gameDtoConverter;
        this.exportService = exportService;
    }

    @GetMapping
    public String getIndexPage(ModelMap model) {
        List<Game> Games = gameService.getAllGames();
        List<GameDto> gamesDtos = new ArrayList<>();
        for (Game game : Games) {
            GameDto gameDto = gameDtoConverter.convert(game);
            gamesDtos.add(gameDto);
        }
        model.addAttribute("games", gamesDtos);
        return "games/index";
    }

    @GetMapping("/new")
    public String getNewPage(ModelMap model) {
        model.addAttribute("winner", WinnerDto.values());
        model.addAttribute("players", playerService.getAllPlayerNames());
        model.addAttribute("roles", RoleDto.values());
        return "games/new";
    }

    @PostMapping("/new")
    public String newGameForm(
            @RequestParam WinnerDto winner, @RequestParam String host, @RequestParam Boolean rating,
            @RequestParam int py, @RequestParam int maf1, @RequestParam int maf2, @RequestParam int maf3,
            @RequestParam String player1, @RequestParam int penalty1, @RequestParam RoleDto role1, @RequestParam double addpoints1,
            @RequestParam String player2, @RequestParam int penalty2, @RequestParam RoleDto role2, @RequestParam double addpoints2,
            @RequestParam String player3, @RequestParam int penalty3, @RequestParam RoleDto role3, @RequestParam double addpoints3,
            @RequestParam String player4, @RequestParam int penalty4, @RequestParam RoleDto role4, @RequestParam double addpoints4,
            @RequestParam String player5, @RequestParam int penalty5, @RequestParam RoleDto role5, @RequestParam double addpoints5,
            @RequestParam String player6, @RequestParam int penalty6, @RequestParam RoleDto role6, @RequestParam double addpoints6,
            @RequestParam String player7, @RequestParam int penalty7, @RequestParam RoleDto role7, @RequestParam double addpoints7,
            @RequestParam String player8, @RequestParam int penalty8, @RequestParam RoleDto role8, @RequestParam double addpoints8,
            @RequestParam String player9, @RequestParam int penalty9, @RequestParam RoleDto role9, @RequestParam double addpoints9,
            @RequestParam String player10, @RequestParam int penalty10, @RequestParam RoleDto role10, @RequestParam double addpoints10) {

        gameService.addNewGame(winner, host, rating, py, maf1, maf2, maf3,
                player1, penalty1, role1, addpoints1, player2, penalty2, role2, addpoints2,
                player3, penalty3, role3, addpoints3, player4, penalty4, role4, addpoints4,
                player5, penalty5, role5, addpoints5, player6, penalty6, role6, addpoints6,
                player7, penalty7, role7, addpoints7, player8, penalty8, role8, addpoints8,
                player9, penalty9, role9, addpoints9, player10, penalty10, role10, addpoints10);
        return "redirect:/games";
    }

    @GetMapping("{game_id}/show")
    public String getInfoPage(@PathVariable int game_id, ModelMap model) {
        Game game = gameService.getGameById(game_id);
        model.addAttribute("game", game);
        return "games/show";
    }

    @GetMapping("/doc")
    public ResponseEntity<byte[]> downloadDoc() {
        String games = getGamesInStringFormat();
        return exportService.downloadDoc(games, "games");
    }

    @GetMapping("/pdf")
    public void downloadPdf(HttpServletResponse response) throws DocumentException, IOException {
        String games = getGamesInStringFormat();
        String filename = "games.pdf";
        byte[] pdf = exportService.preparePdf(games, new Document());
        exportService.downloadFile(response, pdf, filename, MediaType.APPLICATION_PDF);
    }

    @GetMapping("/excel")
    public void downloadExcel(HttpServletResponse response) throws IOException {
        List<Game> allGames = gameService.getAllGames();
        String filename = "games.xls";
        MediaType mediaType = MediaType.valueOf("application/vnd.ms-excel");
        byte[] preparedExcel = exportService.prepareExcel(allGames);
        exportService.downloadFile(response, preparedExcel, filename, mediaType);
    }

    private String getGamesInStringFormat() {
        StringBuilder games = new StringBuilder();
        List<Game> allGames = gameService.getAllGames();
        for (Game game : allGames) {
            games.append(game.toString());
        }
        return games.toString();
    }
}
