package com.geekhub.FINAL_PROJECT.game;

import com.geekhub.FINAL_PROJECT.player.PlayerInGame;

import java.util.List;

public class Game {

    private int id;
    private Winner winner;
    private String host;
    private String date;
    private Boolean rating;
    private Lx lx;
    private List<PlayerInGame> players;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Winner getWinner() {
        return winner;
    }

    public void setWinner(Winner winner) {
        this.winner = winner;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Boolean getRating() {
        return rating;
    }

    public void setRating(Boolean rating) {
        this.rating = rating;
    }

    public Lx getLx() {
        return lx;
    }

    public void setLx(Lx lx) {
        this.lx = lx;
    }

    public List<PlayerInGame> getPlayers() {
        return players;
    }

    public void setPlayers(List<PlayerInGame> players) {
        this.players = players;
    }

    @Override
    public String toString() {
        return "Game{" +
                "id=" + id +
                ", winner=" + winner +
                ", host=" + host +
                ", date='" + date + '\'' +
                ", players=" + players +
                "} \n";
    }
}
