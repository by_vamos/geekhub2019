INSERT INTO games (id, winner, host, date, rating_game)
VALUES (9, 'МАФИЯ', 'Vamos', '2020 APRIL 10', true);

INSERT INTO players_in_game (game_id, player_place, player_name, role, additional_points, lx_points)
VALUES (9, 1, 'Галушка', 'МИРНЫЙ', 0.0, 0.0),
       (9, 2, 'Малифисента', 'МИРНЫЙ', 0.0, 0.0),
       (9, 3, 'Остин', 'МАФИЯ', 0.2, 0.0),
       (9, 4, 'Рауль', 'МАФИЯ', 0.2, 0.0),
       (9, 5, 'Красная Борода', 'МИРНЫЙ', 0.0, 0.0),
       (9, 6, 'Китана', 'МИРНЫЙ', 0.0, 0.25),
       (9, 7, 'Луиза', 'ШЕРИФ', 0.0, 0.0),
       (9, 8, 'Floppy', 'МИРНЫЙ', 0.0, 0.0),
       (9, 9, 'Seezov', 'ДОН', 0.2, 0.0),
       (9, 10, 'Терновик', 'МИРНЫЙ', 0.0, 0.0);

INSERT INTO lx (game_id, py, maf1, maf2, maf3)
VALUES (9, 'Китана', 'Луиза', 'Остин', 'Рауль');