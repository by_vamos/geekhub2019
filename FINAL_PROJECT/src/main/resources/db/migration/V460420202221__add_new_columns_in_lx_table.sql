ALTER TABLE lx
RENAME COLUMN maf To maf1;

ALTER TABLE lx
ADD COLUMN maf2 VARCHAR;

ALTER TABLE lx
ADD COLUMN maf3 VARCHAR;

ALTER TABLE lx
ADD FOREIGN KEY (maf2) REFERENCES players(name);

ALTER TABLE lx
ADD FOREIGN KEY (maf3) REFERENCES players(name);
