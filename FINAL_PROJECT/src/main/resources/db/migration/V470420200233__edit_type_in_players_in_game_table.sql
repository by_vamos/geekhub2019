ALTER TABLE players_in_game
ALTER COLUMN lx_points TYPE NUMERIC(4, 2);

ALTER TABLE players_in_game
ALTER COLUMN additional_points TYPE NUMERIC(4, 2)