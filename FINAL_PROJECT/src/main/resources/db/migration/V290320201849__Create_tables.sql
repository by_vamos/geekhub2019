CREATE TABLE players (
    id SERIAL PRIMARY KEY,
    name VARCHAR NOT NULL UNIQUE
);

CREATE TABLE games (
    id SERIAL PRIMARY KEY,
    winner VARCHAR NOT NULL,
    host VARCHAR,
    date VARCHAR,
    rating_game BOOLEAN DEFAULT FALSE,
    FOREIGN KEY (host) REFERENCES players(name)
);

CREATE TABLE players_in_game (
    id SERIAL PRIMARY KEY,
    game_id INT NOT NULL,
    player_place INT NOT NULL,
    player_name VARCHAR,
    role VARCHAR DEFAULT 'МИРНЫЙ',
    additional_points NUMERIC (2, 1) DEFAULT 0,
    penalty_points NUMERIC (2, 1) DEFAULT 0,
    lx_points NUMERIC (2, 1) DEFAULT 0,
    FOREIGN KEY (game_id) REFERENCES games(id),
    FOREIGN KEY (player_name) REFERENCES players(name)
);

CREATE TABLE lx (
    id SERIAL PRIMARY KEY,
    game_id INT NOT NULL,
    deadman VARCHAR NOT NULL,
    maybe_maf VARCHAR,
    FOREIGN KEY (game_id) REFERENCES games(id),
    FOREIGN KEY (deadman) REFERENCES players(name),
    FOREIGN KEY (maybe_maf) REFERENCES players(name)
);

CREATE TABLE rating (
    id SERIAL PRIMARY KEY,
    player_name VARCHAR,
    kpd NUMERIC,
    total_points NUMERIC DEFAULT 0,
    rated_games INT,
    games INT,
    total_add_points NUMERIC DEFAULT 0,
    total_lx_points NUMERIC DEFAULT 0,
    winrate NUMERIC DEFAULT 0,
    FOREIGN KEY (player_name) REFERENCES players(name)
);