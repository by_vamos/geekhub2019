package com.geekhub.hw2;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        StringBuilder availableShapesBuilder = getAvailableShapesBuilder();
        while (true) {
            System.out.println("Enter selected shape: " + availableShapesBuilder);
            String shape = sc.nextLine();
            AvailableShape availableShape = AvailableShape.of(shape);
            if (availableShape == null) {
                continue;
            }
            switch (availableShape) {
                case CIRCLE:
                    calculateCircle(sc);
                    break;
                case SQUARE:
                    calculateSquare(sc);
                    break;
                case RECTANGLE:
                    calculateRectangle(sc);
                    break;
                case TRIANGLE:
                    calculateTriangle(sc);
                    break;
                default:
                    break;
            }
            break;
        }
    }

    private static void calculateCircle(Scanner sc) {
        System.out.println("Enter r");
        double r = sc.nextFloat();
        Circle circle = new Circle(r);
        System.out.println("Area of this Circle = " + circle.calculateArea());
        System.out.println("Perimeter of this Circle = " + circle.calculatePerimeter());
    }

    private static void calculateTriangle(Scanner sc) {
        System.out.println("Enter a");
        double a = sc.nextFloat();
        System.out.println("Enter b");
        double b = sc.nextFloat();
        System.out.println("Enter c");
        double c = sc.nextFloat();
        Triangle triangle = new Triangle(a, b, c);
        System.out.println("Area of this Triangle = " + triangle.calculateArea());
        System.out.println("Perimeter of this Triangle = " + triangle.calculatePerimeter());
    }

    private static void calculateRectangle(Scanner sc) {
        System.out.println("Enter a");
        double a = sc.nextFloat();
        System.out.println("Enter b");
        double b = sc.nextFloat();
        Rectangle rectangle = new Rectangle(a, b);
        System.out.println("Area of this Rectangle = " + rectangle.calculateArea());
        System.out.println("Perimeter of this Rectangle = " + rectangle.calculatePerimeter());
        System.out.println("Two equivalent Triangles from this Rectangle with Hypotenuse = "
                + rectangle.calculateTriangle());
        Triangle triangle = new Triangle(a, b, rectangle.calculateTriangle());
        System.out.println("Area of this Triangles = " + triangle.calculateArea());
        System.out.println("Perimeter of this Triangles = " + triangle.calculatePerimeter());
    }

    private static void calculateSquare(Scanner sc) {
        System.out.println("Enter a");
        double a = sc.nextFloat();
        Square square = new Square(a);
        System.out.println("Area of this Square = " + square.calculateArea());
        System.out.println("Perimeter of this Square = " + square.calculatePerimeter());
        System.out.println("Two equivalent Triangles from this Square with Hypotenuse = "
                + square.calculateTriangle());
        Triangle triangle = new Triangle(a, a, square.calculateTriangle());
        System.out.println("Area of this Triangles = " + triangle.calculateArea());
        System.out.println("Perimeter of this Triangles = " + triangle.calculatePerimeter());
    }

    private static StringBuilder getAvailableShapesBuilder() {
        AvailableShape[] availableShapes = AvailableShape.values();
        StringBuilder availableShapesBuilder = new StringBuilder();
        for (AvailableShape availableShape : availableShapes) {
            availableShapesBuilder.append(availableShape.name());
            availableShapesBuilder.append(" ");
        }
        return availableShapesBuilder;
    }
}