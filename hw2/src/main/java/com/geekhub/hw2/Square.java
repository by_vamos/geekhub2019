package com.geekhub.hw2;

public class Square implements Shape {
    private final double a;

    Square(double a) {
        this.a = a;
    }

    public double calculateArea() {
        return a * a;
    }

    public double calculatePerimeter() {
        return 4 * a;
    }

    public double calculateTriangle() {
        return Math.sqrt((a * a) + (a * a));
    }
}