package com.geekhub.hw2;

public class Rectangle implements Shape {
    private final double a;
    private final double b;

    Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public double calculateArea() {
        return a * b;
    }

    public double calculatePerimeter() {
        return (a + b) * 2;
    }

    public double calculateTriangle() {
        return Math.sqrt((a * a) + (b * b));
    }
}