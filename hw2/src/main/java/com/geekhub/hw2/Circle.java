package com.geekhub.hw2;

public class Circle implements Shape {
    private final double PI = 3.1415f;
    private final double r;

    Circle(double r) {
        this.r = r;
    }

    public double calculateArea() {
        return PI * r * r;
    }

    public double calculatePerimeter() {
        return 2 * PI * r;
    }
}