package com.geekhub.hw2;

public enum AvailableShape {
    CIRCLE,
    RECTANGLE,
    SQUARE,
    TRIANGLE;

    public static AvailableShape of(String code) {
        AvailableShape[] values = values();
        for (AvailableShape value : values) {
            if (value.name().equals(code.toUpperCase())) {
                return value;
            }
        }
        return null;
    }
}