package com.geekhub.hw2;

import org.junit.Test;

import static org.junit.Assert.*;

public class SquareTest {

    @Test
    public void calculateArea() {
        Square square = new Square(3);
        double area = square.calculateArea();
        double res = 9;
        assertEquals(area, res, 1);
    }

    @Test
    public void calculatePerimeter() {
        Square square = new Square(3);
        double perimeter = square.calculatePerimeter();
        double res = 12;
        assertEquals(perimeter, res, 1);
    }

    @Test
    public void calculateTriangle() {
        Square square = new Square(3);
        double triangle = square.calculateTriangle();
        double res = 4.24;
        assertEquals(triangle, res, 1);
    }
}