package com.geekhub.hw2;

import org.junit.Test;

import static org.junit.Assert.*;

public class CircleTest {


    @Test
    public void calculateArea() {
        Circle circle = new Circle(3);
        double area = circle.calculateArea();
        double res = 28.2735;
        assertEquals(area, res, 1);
    }

    @Test
    public void calculatePerimeter() {
        Circle circle = new Circle(3);
        double perimeter = circle.calculatePerimeter();
        double res = 18.849;
        assertEquals(perimeter, res, 1);
    }
}