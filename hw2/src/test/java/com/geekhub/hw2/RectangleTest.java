package com.geekhub.hw2;

import org.junit.Test;

import static org.junit.Assert.*;

public class RectangleTest {

    @Test
    public void calculateArea() {
        Rectangle rectangle = new Rectangle(3, 4);
        double area = rectangle.calculateArea();
        double res = 12;
        assertEquals(area, res, 1);
    }

    @Test
    public void calculatePerimeter() {
        Rectangle rectangle = new Rectangle(3, 4);
        double perimeter = rectangle.calculatePerimeter();
        double res = 14;
        assertEquals(perimeter, res, 1);
    }

    @Test
    public void calculateTriangle() {
        Rectangle rectangle = new Rectangle(3, 4);
        double triangle = rectangle.calculateTriangle();
        double res = 5;
        assertEquals(triangle, res, 1);
    }
}