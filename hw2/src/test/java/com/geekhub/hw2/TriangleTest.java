package com.geekhub.hw2;

import org.junit.Test;

import static org.junit.Assert.*;

public class TriangleTest {

    public Triangle triangle = new Triangle(3, 4, 5);

    @Test
    public void calculateArea() {
        double area = triangle.calculateArea();
        double res = 6;
        assertEquals(area, res, 1);
    }

    @Test
    public void calculatePerimeter() {
        double perimeter = triangle.calculatePerimeter();
        double res = 12;
        assertEquals(perimeter, res, 1);
    }
}