package com.geekgub.hw7.task1_3;

import java.util.Objects;

public class Car {

    private int maxCount;
    private String model;
    private String type;
    private String color;
    private int maxSpeed;

    public Car(int maxCount, String model, String type, String color, int maxSpeed) {
        this.maxCount = maxCount;
        this.model = model;
        this.type = type;
        this.color = color;
        this.maxSpeed = maxSpeed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return maxCount == car.maxCount &&
                maxSpeed == car.maxSpeed &&
                model.equals(car.model) &&
                type.equals(car.type) &&
                color.equals(car.color);
    }

    @Override
    public int hashCode() {
        return Objects.hash(maxCount, model, type, color, maxSpeed);
    }
}
