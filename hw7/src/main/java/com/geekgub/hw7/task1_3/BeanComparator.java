package com.geekgub.hw7.task1_3;

import java.lang.reflect.Field;

public class BeanComparator {
    public static void main(String[] args) throws IllegalAccessException {
        Car car1 = new Car(120, "RX-7", "coupe", "black", 230);
        Car car2 = new Car(120, "RX-8", "sedan", "black", 225);

        getCompare(car1, car2);
    }

    private static <T> void getCompare(T object1, T object2) throws IllegalAccessException {
        Class clazz = object1.getClass();
        Field[] clazzFields = clazz.getDeclaredFields();
        for (Field clazzField : clazzFields) {
            clazzField.setAccessible(true);
            boolean match = clazzField.get(object1).equals(clazzField.get(object2));
            System.out.println(clazzField.getName() + " " +
                    clazzField.get(object1) + " " +
                    clazzField.get(object2) + " " +
                    match);
        }
    }
}
