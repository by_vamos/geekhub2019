package com.geekgub.hw7.task1_2;

import com.geekgub.hw7.task1_1.Car;
import com.geekgub.hw7.task1_1.Cat;
import com.geekgub.hw7.task1_1.Human;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

public class CloneCreator {
    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException,
            InstantiationException, IllegalAccessException {
        Car car = new Car("black", 190, "Sedan", "RX-7");
        Cat cat = new Cat("Black", 3, 4, 35);
        Human human = new Human(180, "male", 22, 75);

        System.out.println(clone(car, car.getClass()));
        System.out.println(clone(cat, cat.getClass()));
        System.out.println(clone(human, human.getClass()));
    }

    private static <T> String clone(Object o, T clazz) throws NoSuchMethodException, IllegalAccessException,
            InvocationTargetException, InstantiationException {
        ArrayList<Object> params = new ArrayList<>();
        Class toClone = (Class) clazz;
        Class[] parameterTypes = null;
        Constructor[] constructors = toClone.getConstructors();
        for (Constructor constructor : constructors) {
            parameterTypes = constructor.getParameterTypes();
        }
        Field[] toDeclaredFields = toClone.getDeclaredFields();
        for (Field toDeclaredField : toDeclaredFields) {
            toDeclaredField.setAccessible(true);
            params.add(toDeclaredField.get(o));
        }
        T clone = (T) toClone.getConstructor(parameterTypes).newInstance(params.toArray());
        return "Clone: " + clone.toString();
    }
}

