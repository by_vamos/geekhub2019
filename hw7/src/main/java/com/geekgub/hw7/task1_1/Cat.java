package com.geekgub.hw7.task1_1;

public class Cat {

    private String color;
    private int age;
    private int legCount;
    private int fullLength;

    public Cat(String color, int age, int legCount, int fullLength) {
        this.color = color;
        this.age = age;
        this.legCount = legCount;
        this.fullLength = fullLength;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "color='" + color + '\'' +
                ", age=" + age +
                ", legCount=" + legCount +
                ", fullLength=" + fullLength +
                '}';
    }
}
