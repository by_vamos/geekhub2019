package com.geekgub.hw7.task1_1;

import java.lang.reflect.Field;

public class BeanRepresenter {
    public static void main(String[] args) throws IllegalAccessException {
        Car car = new Car("black", 190, "Sedan", "RX-7");
        Cat cat = new Cat("Black", 3, 4, 35);
        Human human = new Human(180, "male", 22, 75);

        getRepresent(car);
        getRepresent(cat);
        getRepresent(human);
    }

    private static void getRepresent(Object object) throws IllegalAccessException {
        Class toRepresent = object.getClass();
        Field[] toDeclaredFields = toRepresent.getDeclaredFields();
        for (Field toDeclaredField : toDeclaredFields) {
            toDeclaredField.setAccessible(true);
            System.out.println(toDeclaredField.getName() + " " + toDeclaredField.get(object));
        }
        System.out.println();
    }
}
