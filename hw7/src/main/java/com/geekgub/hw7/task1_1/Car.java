package com.geekgub.hw7.task1_1;

public class Car {

    private String color;
    private int maxSpeed;
    private String type;
    private String model;

    public Car(String color, int maxSpeed, String type, String model) {
        this.color = color;
        this.maxSpeed = maxSpeed;
        this.type = type;
        this.model = model;
    }

    @Override
    public String toString() {
        return "Car{" +
                "color='" + color + '\'' +
                ", maxSpeed=" + maxSpeed +
                ", type='" + type + '\'' +
                ", model='" + model + '\'' +
                '}';
    }
}
