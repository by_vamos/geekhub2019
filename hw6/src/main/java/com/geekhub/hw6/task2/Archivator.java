package com.geekhub.hw6.task2;

import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

class Archivator {
    private File destination;
    private Map<String, List<File>> filesMap;

    Archivator(File destination, Map<String, List<File>> filesMap) {
        this.destination = destination;
        this.filesMap = filesMap;
    }

    void archiving() throws IOException {
        File audioArchive = getAudioArchive();
        File videoArchive = getVideoArchive();
        File imageArchive = getImageArchive();
        List<File> audioFiles = filesMap.get("audio");
        List<File> videoFiles = filesMap.get("video");
        List<File> imageFiles = filesMap.get("image");

        zipFiles(audioArchive, audioFiles);
        assert videoArchive != null;
        zipFiles(videoArchive, videoFiles);
        assert imageArchive != null;
        zipFiles(imageArchive, imageFiles);
    }

    private void zipFiles(File audioArchive, List<File> audioFiles) throws IOException {
        try (ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(audioArchive))) {
            for (File audioFile : audioFiles) {
                ZipEntry entry = new ZipEntry(audioFile.getPath());
                zos.putNextEntry(entry);
                FileInputStream fis = new FileInputStream(audioFile);
                write(zos, fis);
                fis.close();
            }
        }
    }

    private void write(ZipOutputStream zos, FileInputStream fis) throws IOException {
        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zos.write(bytes, 0, length);
        }
    }

    private File getAudioArchive() throws IOException {
        File audios = new File("hw6/src/main/resources/zipFileMakerResult/audios.zip");
        audios.createNewFile();
        return audios;
    }

    private File getVideoArchive() throws IOException {
        File[] archives = destination.listFiles();
        assert archives != null;
        for (File archive : archives) {
            if (archive.getName().contains("videos.zip")) {
                return archive;
            } else {
                File videos = new File("hw6/src/main/resources/zipFileMakerResult/videos.zip");
                videos.createNewFile();
                return videos;
            }
        }
        return null;
    }

    private File getImageArchive() throws IOException {
        File[] archives = destination.listFiles();
        assert archives != null;
        for (File archive : archives) {
            if (archive.getName().contains("images.zip")) {
                return archive;
            } else {
                File images = new File("hw6/src/main/resources/zipFileMakerResult/images.zip");
                images.createNewFile();
                return images;
            }
        }
        return null;
    }
}