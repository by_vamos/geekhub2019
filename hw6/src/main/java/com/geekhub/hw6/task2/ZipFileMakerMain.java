package com.geekhub.hw6.task2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class ZipFileMakerMain {
    public static void main(String[] args) throws IOException {
        String inputDir = "hw6/src/main/resources/zipFileMaker";
        File input = new File(inputDir);
        File output = new File("hw6/src/main/resources/zipFileMakerResult");
        File[] listFiles = input.listFiles();
        assert listFiles != null;
        ArrayList<File> files = new ArrayList<>(Arrays.asList(listFiles));
        FilterData filter = new FilterData(files);
        Archivator archivator = new Archivator(output, filter.writeDataToMap());
        archivator.archiving();
    }
}
