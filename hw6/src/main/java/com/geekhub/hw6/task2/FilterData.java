package com.geekhub.hw6.task2;

import java.io.File;
import java.util.*;

class FilterData {

    private List<File> files;

    FilterData(List<File> files) {
        this.files = files;
    }

    Map<String, List<File>> writeDataToMap() {
        ArrayList<File> insideList = new ArrayList<>();
        Map<String, List<File>> insideMap = new HashMap<>();
        ArrayList<File> audioList = new ArrayList<>();
        ArrayList<File> videoList = new ArrayList<>();
        ArrayList<File> imageList = new ArrayList<>();

        for (File file : files) {
            if (!file.isDirectory()) {
                identifyFileAndAddInList(audioList, videoList, imageList, file);
            } else {
                List<String> filesPathList = getFilesPathList(file);
                for (String pathName : filesPathList) {
                    File fileInDirectory = new File(pathName);
                    insideList.add(fileInDirectory);
                }
            }
        }
        for (File file : insideList) {
            identifyFileAndAddInList(audioList, videoList, imageList, file);
        }

        insideMap.put("audio", audioList);
        insideMap.put("video", videoList);
        insideMap.put("image", imageList);

        return insideMap;
    }

    private void identifyFileAndAddInList(ArrayList<File> audioList, ArrayList<File> videoList, ArrayList<File> imageList, File file) {
        if (isAudio(file)) {
            audioList.add(file);
        }
        if (isVideo(file)) {
            videoList.add(file);
        }
        if (isImage(file)) {
            imageList.add(file);
        }
    }


    private boolean isAudio(File file) {
        String fileName = file.getName();
        return fileName.contains(".mp3") || fileName.contains(".wma") || fileName.contains(".wav");
    }

    private boolean isVideo(File file) {
        String fileName = file.getName();
        return fileName.contains(".mp4") || fileName.contains(".avi") || fileName.contains(".flv");
    }

    private boolean isImage(File file) {
        String fileName = file.getName();
        return fileName.contains(".jpeg") || fileName.contains(".jpg") ||
                fileName.contains(".gif") || fileName.contains(".png");
    }

    private List<String> getFilesPathList(File file) {
        List<String> insidePathList = new ArrayList<>();
        File[] filesListInDirectory = file.listFiles();
        assert filesListInDirectory != null;
        for (File fileInDirectory : filesListInDirectory) {
            if (fileInDirectory.isDirectory()) {
                insidePathList.addAll(getFilesPathList(fileInDirectory));
            } else {
                insidePathList.add(fileInDirectory.getPath());
            }
        }
        return insidePathList;
    }
}