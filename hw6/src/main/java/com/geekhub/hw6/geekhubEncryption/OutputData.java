package com.geekhub.hw6.geekhubEncryption;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class OutputData {
    private File file;
    private List<String> resultAfterTransform;

    OutputData(File file, List<String> resultAfterTransform) {
        this.file = file;
        this.resultAfterTransform = resultAfterTransform;
    }

    private List<String> createListToOutput() {
        List<String> insideList = new ArrayList<>();
        StringBuilder line = new StringBuilder();
        for (String word : resultAfterTransform) {
            if (!word.equals("/")) {
                line.append(word);
                line.append(" ");
            } else {
                line.append('\n');
            }
        }
        insideList.add(line.toString());
        return insideList;
    }

    void writeResult() throws IOException {
        List<String> result = createListToOutput();
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            for (String line : result) {
                writer.write(line);
            }
            writer.flush();
        }
    }
}