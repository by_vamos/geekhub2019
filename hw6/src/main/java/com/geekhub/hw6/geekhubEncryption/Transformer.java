package com.geekhub.hw6.geekhubEncryption;

import java.util.ArrayList;
import java.util.List;

class Transformer {
    private List<String> list;

    Transformer(List<String> list) {
        this.list = list;
    }

    List<String> transformData() {
        List<String> insideList = new ArrayList<>();
        StringBuilder result;
        for (String word : list) {
            StringBuilder sb = new StringBuilder(word);
            int wordLength = word.length();
            if (wordLength >= 10) {
                if (checkLastCharacter(word)) {
                    result = sb.replace(1, sb.length() - 2, Integer.toString(wordLength - 3));
                } else {
                    result = sb.replace(1, sb.length() - 1, Integer.toString(wordLength - 2));
                }
                insideList.add(result.toString());
            } else {
                insideList.add(word);
            }
        }
        return insideList;
    }

    private boolean checkLastCharacter(String word) {
        StringBuilder sb = new StringBuilder(word);
        Character last = sb.charAt(sb.length() - 1);
        return last.equals(',') || last.equals('.');
    }
}