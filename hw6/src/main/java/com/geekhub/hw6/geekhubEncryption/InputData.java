package com.geekhub.hw6.geekhubEncryption;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class InputData {
    private File file;

    InputData(File input) {
        file = input;
    }

    private List<String> createLinesList() throws IOException {
        List<String> insideLinesList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String s;
            while ((s = reader.readLine()) != null)
                insideLinesList.add(s);
            return insideLinesList;
        }
    }

    List<String> createWordsList() throws IOException {
        List<String> linesList = createLinesList();
        List<String> wordsList = new ArrayList<>();
        for (String line : linesList) {
            List<String> insideWordsList = new ArrayList<>(Arrays.asList(line.split(" ")));
            wordsList.addAll(insideWordsList);
            wordsList.add("/");
        }
        return wordsList;
    }
}
