package com.geekhub.hw6.geekhubEncryption;

import java.io.File;
import java.io.IOException;

public class Encryption {
    public static void main(String[] args) throws IOException {
        File input = new File("hw6/src/main/resources/geekhubEncryption/before.txt");
        File output = new File("hw6/src/main/resources/geekhubEncryption/after.txt");
        InputData inputdata = new InputData(input);
        Transformer transformer = new Transformer(inputdata.createWordsList());
        OutputData outputData = new OutputData(output, transformer.transformData());
        outputData.writeResult();
    }
}
