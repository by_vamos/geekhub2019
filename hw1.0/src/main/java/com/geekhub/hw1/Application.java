package com.geekhub.hw1;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        long phone;
        long phoneArrSum = 0;
        long phoneDigits = 0;
        long phoneByte = 0;

        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("Enter the number start with 380:");
            phone = sc.nextLong();
            if (phone > 380000000000L) {
                System.out.println("Thank You! Your number: " + phone);
                break;
            } else {
                System.out.println("Enter the number in the format 38093ХХХХХХХ");
            }
        }
        phoneArrSum = getSum(phone, phoneArrSum);
        System.out.println("The sum of all digits of your phone number (Task 1): " + phoneArrSum);
        phoneDigits = getSum(phoneArrSum, phoneDigits);
        System.out.println("You phone number in way to 1 digit (Task 2): " + phoneDigits);
        phoneByte = getSum(phoneDigits, phoneByte);
        System.out.println("You phone number in 1 digit (Task 3): " + phoneByte);
        switch ((int) phoneByte) {
            case 1:
                System.out.println("You phone number in 1 word if lower then 4 (Task 4): One");
                break;
            case 2:
                System.out.println("You phone number in 1 word if lower then 4 (Task 4): Two");
                break;
            case 3:
                System.out.println("You phone number in 1 word if lower then 4 (Task 4): Three");
                break;
            case 4:
                System.out.println("You phone number in 1 word if lower then 4 (Task 4): Four");
                break;
            default:
                System.out.println("You phone number in 1 word if lower then 4 (Task 4): " + phoneByte);
        }
    }

    private static long getSum(long input, long sum) {
        while (input > 0) {
            sum = sum + (input % 10);
            input = input / 10;
        }
        return sum;
    }
}