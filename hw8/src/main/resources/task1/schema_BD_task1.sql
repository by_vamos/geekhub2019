CREATE TABLE "customer"
(
    id        int PRIMARY KEY,
    firstName VARCHAR(30),
    lastName  VARCHAR(30),
    cellPhone VARCHAR(15)
);

CREATE TABLE "product"
(
    id           int PRIMARY KEY,
    name         VARCHAR(30),
    description  VARCHAR(30),
    currentPrice float(2)
);

CREATE TABLE "order"
(
    id              int PRIMARY KEY,
    customerId      int,
    productId       int,
    productQuantity int,
    deliveryPlace   VARCHAR(30),
    foreign key (customerId) references customer (id),
    foreign key (productId) references product (id)
);