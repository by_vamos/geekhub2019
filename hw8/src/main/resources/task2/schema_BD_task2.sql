create TABLE "cat"
(
    id   SERIAL PRIMARY KEY,
    name VARCHAR(30),
    age  INT
);

create TABLE "user"
(
    id      SERIAL PRIMARY KEY,
    name    VARCHAR(30),
    age     INT,
    admin   BOOLEAN default true,
    balance float(2)
);