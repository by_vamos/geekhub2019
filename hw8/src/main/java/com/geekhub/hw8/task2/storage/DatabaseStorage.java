package com.geekhub.hw8.task2.storage;

import com.geekhub.hw8.task2.objects.Entity;
import com.geekhub.hw8.task2.objects.Ignore;

import java.lang.reflect.*;
import java.sql.*;
import java.util.*;

/**
 * Implementation of {@link Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link Entity} class.
 * Could be created only with {@link Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        //this method is fully implemented, no need to do anything, it's just an example
        String sql = "SELECT * FROM \"" + clazz.getSimpleName().toLowerCase() + "\" WHERE id = " + id;
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        String sql = "SELECT * FROM " + clazz.getSimpleName().toLowerCase();
        try (Statement statement = connection.createStatement()) {
            return extractResult(clazz, statement.executeQuery(sql));
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        String columnName = entity.getClass().getSimpleName().toLowerCase();
        try (Statement statement = connection.createStatement()) {
            int result = statement.executeUpdate("DELETE FROM \"" + columnName + "\" WHERE id = " + entity.getId());
            if (result == 1) return true;
        }
        return false;
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        String tableName = entity.getClass().getSimpleName().toLowerCase();
        Map<String, Object> preparedEntity = prepareEntity(entity);
        if (entity.isNew()) {
            insertEntity(entity, tableName, preparedEntity);
        } else {
            updateEntity(entity, tableName, preparedEntity);
        }
    }

    private <T extends Entity> void updateEntity(T entity, String tableName, Map<String, Object> preparedEntity) throws SQLException {
        for (String key : preparedEntity.keySet()) {
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate("UPDATE \"" + tableName + "\" SET " + key + " = '" + preparedEntity.get(key) +
                        "' WHERE id = " + entity.getId());
            }
        }
    }

    private <T extends Entity> void insertEntity(T entity, String tableName, Map<String, Object> preparedEntity)
            throws SQLException {
        ResultSet generatedKeys;
        String nameEntity = (String) preparedEntity.get("name");
        try (Statement stmt = connection.createStatement()) {
            stmt.executeUpdate("INSERT INTO \"" + tableName + "\" (name) VALUES ('" + nameEntity + "')",
                    Statement.RETURN_GENERATED_KEYS);
            generatedKeys = stmt.getGeneratedKeys();
            while (generatedKeys.next())
                entity.setId(generatedKeys.getInt(1));
        }
        updateEntity(entity, tableName, preparedEntity);
    }

    //converts object to map, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        Map<String, Object> data = new HashMap<>();
        for (Field field : entity.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(Ignore.class)) continue;
            field.setAccessible(true);
            data.put(field.getName(), field.get(entity));
        }
        return data;
    }

    //creates list of new instances of clazz by using data from resultset
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultSet) throws Exception {
        List<T> resultList = new ArrayList<>();
        while (resultSet.next()) {
            T object = clazz.newInstance();
            for (Field field : clazz.getDeclaredFields()) {
                if (field.isAnnotationPresent(Ignore.class)) continue;
                field.setAccessible(true);
                objectFilling(clazz, resultSet, object, field.getName(), field.getGenericType());
            }
            for (Field field : clazz.getSuperclass().getDeclaredFields()) {
                field.setAccessible(true);
                Class superClazz = clazz.getSuperclass();
                objectFilling(superClazz, resultSet, object, "id", field.getGenericType());
            }
            resultList.add(object);
        }
        return resultList;
    }

    private <T extends Entity> void objectFilling(Class<T> clazz, ResultSet resultSet, T object, String fieldName, Type fieldType)
            throws SQLException, IllegalAccessException, InvocationTargetException {
        switch (fieldType.getTypeName().toLowerCase()) {
            case "java.lang.string":
                String stringValue = resultSet.getString(fieldName);
                setValue(clazz, object, fieldName, stringValue);
                break;
            case "int":
            case "java.lang.integer":
                int intValue = resultSet.getInt(fieldName);
                setValue(clazz, object, fieldName, intValue);
                break;
            case "java.lang.boolean":
                boolean booleanValue = resultSet.getBoolean(fieldName);
                setValue(clazz, object, fieldName, booleanValue);
                break;
            case "java.lang.double":
                double doubleValue = resultSet.getDouble(fieldName);
                setValue(clazz, object, fieldName, doubleValue);
                break;
        }
    }

    private <T extends Entity, V> void setValue(Class<T> clazz, T object, String fieldName, V value)
            throws IllegalAccessException, InvocationTargetException {
        String methodName = "set" + fieldName;
        Method[] declaredMethods = clazz.getDeclaredMethods();
        for (Method method : declaredMethods) {
            if (method.getName().toLowerCase().equals(methodName)) {
                method.invoke(object, value);
            }
        }
    }
}
