-- 1.
SELECT c.id,
       c.sortname,
       c.name,
       c.phonecode,
       count(s.id) countstates
FROM countries c,
     states s
WHERE c.id = s.country_id
GROUP BY c.id
ORDER BY countstates DESC
LIMIT 1;

-- 2.
SELECT c.id,
       c.sortname,
       c.name,
       c.phonecode,
       count(c2.id) citiescount
FROM countries c
         JOIN states s ON c.id = s.country_id
         JOIN cities c2 ON s.id = c2.state_id
GROUP BY c.id
ORDER BY citiescount DESC
LIMIT 1;

-- 3.
SELECT c.id,
       c.sortname,
       c.name,
       c.phonecode,
       count(s.id) countstates
FROM countries c,
     states s
WHERE c.id = s.country_id
GROUP BY c.id, c.name
ORDER BY countstates DESC, c.name, c.id;

-- 4.
SELECT c.id,
       c.sortname,
       c.name,
       c.phonecode,
       count(c2.id) citiescount
FROM countries c
         JOIN states s ON c.id = s.country_id
         LEFT JOIN cities c2 on s.id = c2.state_id
GROUP BY c.id, c.name
ORDER BY citiescount DESC, c.name, c.id;

-- 5.
SELECT c.id,
       c.sortname,
       c.name,
       c.phonecode,
       s.count countstates,
       table3.citiescount
FROM countries c
         JOIN (SELECT country_id, count(id) count FROM states GROUP BY country_id) s ON c.id = s.country_id
         LEFT JOIN (SELECT c.id,
                           count(c2.id) citiescount
                    FROM countries c
                             JOIN states s ON c.id = s.country_id
                             JOIN cities c2 ON s.id = c2.state_id
                    GROUP BY c.id) table3 ON table3.id = s.country_id
ORDER BY countstates DESC, table3.citiescount DESC;

-- 6.
SELECT c.id,
       c.sortname,
       c.name,
       c.phonecode
FROM countries c
WHERE c.id IN (
    SELECT s.country_id
    FROM states s
             LEFT JOIN (SELECT state_id, count(id) citycount FROM cities GROUP BY state_id) AS s2 ON s.id = s2.state_id
    ORDER BY s2.citycount DESC NULLS LAST
    LIMIT 10);

-- 7.
SELECT *
FROM (SELECT c.id,
             c.sortname,
             c.name,
             c.phonecode,
             count(s.id) countstates
      FROM countries c,
           states s
      WHERE c.id = s.country_id
      GROUP BY c.id, c.name
      ORDER BY countstates DESC
      LIMIT 10) as countmax
UNION
SELECT *
FROM (SELECT c.id,
             c.sortname,
             c.name,
             c.phonecode,
             count(s.id) countstates
      FROM countries c,
           states s
      WHERE c.id = s.country_id
      GROUP BY c.id, c.name
      ORDER BY countstates
      LIMIT 10) as countmin
ORDER BY countstates DESC, name;

-- 8.
SELECT c.id,
       c.sortname,
       c.name,
       c.phonecode,
       count(s.id) countstates
FROM countries c,
     states s
WHERE c.id = s.country_id
GROUP BY c.id
HAVING count(s.id) > (SELECT avg(count)
                      FROM (SELECT count(id) count FROM states s GROUP BY country_id) AS countall)
ORDER BY countstates;

-- 9.
SELECT DISTINCT ON (countstates) *
FROM (SELECT c.id,
             c.sortname,
             c.name,
             c.phonecode,
             count(s.id) countstates
      FROM countries c,
           states s
      WHERE c.id = s.country_id
      GROUP BY c.id, c.name
      ORDER BY countstates, name) subquery;

-- 10.
SELECT s.name
FROM states s
    EXCEPT ALL
SELECT DISTINCT s.name
FROM states s;

-- 11.
SELECT s.id,
       s.name,
       s.country_id,
       s2.citycount
FROM states s
         LEFT JOIN (SELECT state_id, count(id) citycount FROM cities group by state_id) AS s2 ON s.id = s2.state_id
WHERE s2.citycount IS NULL
