-- 1. Count sum of tickets price per each train
SELECT tr.train_id,
       t.number,
       t.type,
       t.carriages,
       t.seats,
       sum(tic.price_uah) totalvalue
FROM tickets tic
         RIGHT JOIN trips tr ON tic.trip_id = tr.id
         JOIN trains t ON tr.train_id = t.id
GROUP BY tr.train_id, t.number, t.type, t.carriages, t.seats
ORDER BY tr.train_id;

-- 2. Number of tickets sold per each trip
SELECT t.id,
       t.train_id,
       t.departure,
       t.arrival,
       count(tickets.id) solded_tickets
FROM tickets
         RIGHT JOIN trips t ON tickets.trip_id = t.id
GROUP BY t.id, t.train_id, t.departure, t.arrival
ORDER BY t.id;

-- 3. Count how much passengers departure at night
SELECT count(id) departure_at_night
FROM tickets
WHERE trip_id IN
      (SELECT id
       FROM trips
       WHERE departure_time > '22:00:00'
       UNION
       SELECT id
       FROM trips
       WHERE departure_time < '06:00:00');

-- 4. Select how much tickets left per each trip
SELECT tr.id,
       tr.train_id,
       tr.departure,
       tr.arrival,
       tr.departure_time,
       tr.arrival_time,
       t.seats - s.solded AS left_tickets,
       s.solded
FROM trips tr
         JOIN (SELECT *
               FROM trains) t ON train_id = t.id
         LEFT JOIN (SELECT trip_id, count(id) solded
                    FROM tickets
                    GROUP BY trip_id
                    ORDER BY trip_id) s ON tr.id = s.trip_id
WHERE s.solded IS NOT NULL
UNION
SELECT tr.id,
       tr.train_id,
       tr.departure,
       tr.arrival,
       tr.departure_time,
       tr.arrival_time,
       t.seats AS left_tickets,
       s.solded
FROM trips tr
         JOIN (SELECT *
               FROM trains) t ON train_id = t.id
         LEFT JOIN (SELECT trip_id, count(id) solded
                    FROM tickets
                    GROUP BY trip_id
                    ORDER BY trip_id) s ON tr.id = s.trip_id
WHERE s.solded IS NULL
ORDER BY id;

-- 5. Show purchased tickets
SELECT passenger.fullname,
       passenger.passport,
       tickets.carriage_number,
       tickets.seat_number,
       trip.departure,
       trip.arrival,
       trip.departure_time,
       trip.arrival_time
FROM tickets
         JOIN (SELECT id,
                      name || ' ' || surname AS fullname,
                      passport
               FROM passengers) passenger ON passenger_id = passenger.id
         JOIN (SELECT id,
                      departure,
                      arrival,
                      departure_time,
                      arrival_time
               FROM trips) trip ON trip.id = tickets.trip_id
ORDER BY trip_id;

-- 6. Adding 3 carriages to comfort trains
UPDATE trains
SET carriages = carriages + 3,
    seats     = seats + 108
WHERE type = 'Comfort'
  AND carriages < 13
RETURNING *;

-- 7. Adding 15% discount on all destinations from Kiev
UPDATE tickets
SET price_uah = price_uah * 0.85
WHERE trip_id IN
      (SELECT id
       FROM trips
       WHERE departure = 'Kiev')
RETURNING *;

-- 8. Trains from Kiev changed the schedule
UPDATE trips
SET departure_time = departure_time + '01:00:00',
    arrival_time = arrival_time + '01:00:00'
WHERE departure = 'Kiev'
RETURNING *;

-- 9. Delete trips with 0 tickets sold
DELETE
FROM trips
    USING (SELECT tr.id,
                  tr.train_id,
                  tr.departure,
                  tr.arrival,
                  tr.departure_time,
                  tr.arrival_time,
                  t.seats AS left_tickets,
                  s.solded
           FROM trips tr
                    JOIN (SELECT *
                          FROM trains) t ON train_id = t.id
                    LEFT JOIN (SELECT trip_id, count(id) solded
                               FROM tickets
                               GROUP BY trip_id
                               ORDER BY trip_id) s ON tr.id = s.trip_id
           WHERE s.solded IS NULL) del
WHERE trips.id = del.id
RETURNING *;

-- 10. Delete tickets from Poltava
DELETE
FROM tickets
    USING (SELECT tic.id,
                  t.departure
           FROM tickets tic
                    JOIN trips t on tic.trip_id = t.id
           WHERE departure = 'Poltava') del
WHERE tickets.id = del.id
RETURNING *;