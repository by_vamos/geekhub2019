CREATE TABLE IF NOT EXISTS "passengers"
(
    "id" SERIAL PRIMARY KEY,
    "name" VARCHAR(30) NOT NULL,
    "surname" VARCHAR(30) NOT NULL,
    "passport" VARCHAR(8) UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS "trains"
(
    "id" SERIAL PRIMARY KEY,
    "number" INT UNIQUE,
    "type" VARCHAR(30) NOT NULL,
    "carriages" INT NOT NULL,
    "seats" INT NOT NULL
);

CREATE TABLE IF NOT EXISTS "trips"
(
    "id" SERIAL PRIMARY KEY,
    "train_id" INT REFERENCES trains (id) NOT NULL,
    "departure" VARCHAR(30) NOT NULL,
    "arrival" VARCHAR(30) NOT NULL,
    "departure_time" TIME(0) NOT NULL,
    "arrival_time" TIME(0) NOT NULL
);

CREATE TABLE IF NOT EXISTS "tickets"
(
    "id" SERIAL PRIMARY KEY,
    "passenger_id" INT REFERENCES passengers (id) NOT NULL,
    "trip_id" INT REFERENCES trips (id) NOT NULL,
    "carriage_number" INT NOT NULL,
    "seat_number" INT NOT NULL,
    "price_uah" REAL NOT NULL
);

INSERT INTO passengers (id, name, surname, passport) VALUES
(1, 'Alexandr', 'Petrov', 'HE095356'),
(2, 'Sergey', 'Burunov', 'HE052417'),
(3, 'Henry', 'Cavill', 'AB217643'),
(4, 'Anya', 'Chalotra', 'NU487603'),
(5, 'Freya', 'Allan', 'QO321096'),
(6, 'Joey', 'Batey', 'PO045729'),
(7, 'Anna', 'Buring', 'QO960471'),
(8, 'Eamon', 'Farren', 'GJ302762'),
(9, 'Mimi', 'Ndiweni', 'KG219542'),
(10, 'Adam', 'Levy', 'KJ683910'),
(11, 'Mahesh', 'Jadu', 'AS219632');

INSERT INTO trains (id, number, type, carriages, seats) VALUES
(1, 146, 'Standart', 21, 1062),
(2, 58, 'Standart', 19, 846),
(3, 27, 'Comfort', 8, 288),
(4, 229, 'Standart', 23, 1246),
(5, 185, 'Econom', 21, 1182),
(6, 186, 'Comfort', 11, 396),
(7, 19, 'Standart', 14, 684),
(8, 76, 'Comfort', 14, 458),
(9, 96, 'Econom', 18, 980),
(10, 83, 'Standart', 18, 870);

INSERT INTO trips (id, train_id, departure, arrival, departure_time, arrival_time) VALUES
(1, 1, 'Cherkasy', 'Odessa', '20:17:00', '05:14:00'),
(2, 6, 'Kharkiv', 'Lutsk', '18:44:00', '23:51:00'),
(3, 10, 'Lviv', 'Vinnytsya', '13:37:00', '21:46:00'),
(4, 4, 'Kiev', 'Kherson', '20:56:00', '14:54:00'),
(5, 3, 'Kiev', 'Rivne', '03:23:00', '11:28:00'),
(6, 1, 'Poltava', 'Zhytomyr', '23:16:00', '05:57:00'),
(7, 8, 'Dnepr', 'Zaporizhzhya', '17:12:00', '20:11:00'),
(8, 3, 'Kiev', 'Simferopol', '00:26:00', '04:13:00'),
(9, 7, 'Cherkasy', 'Chernihiv', '13:56:00', '19:24:00'),
(10, 2, 'Ternopil', 'Ivano-Frankivsk', '17:38:00', '19:01:00'),
(11, 5, 'Ternopil', 'Chernihiv', '19:38:00', '05:41:00'),
(12, 2, 'Ternopil', 'Kirovograd', '23:18:00', '03:18:00'),
(13, 5, 'Odessa', 'Luhansk', '03:56:00', '19:26:00'),
(14, 8, 'Kiev', 'Kharkiv', '08:41:00', '14:56:00'),
(15, 10, 'Kharkiv', 'Sumy', '09:52:00', '13:23:00');

INSERT INTO tickets (id, passenger_id, trip_id, carriage_number, seat_number, price_uah) VALUES
(1, 2, 1, 8, 47, 151.58),
(2, 8, 2, 3, 23, 156.23),
(3, 6, 7, 11, 45, 200.18),
(4, 1, 5, 16, 12, 91.29),
(5, 5, 4, 19, 3, 134.67),
(6, 9, 7, 8, 7, 230.11),
(7, 3, 4, 5, 18, 102.87),
(8, 7, 7, 12, 46, 167.98),
(9, 6, 9, 2, 25, 145.11),
(10, 4, 2, 17, 23, 137.23),
(11, 3, 2, 2, 11, 144.11),
(12, 7, 5, 11, 48, 112.76),
(13, 9, 10, 7, 51, 96.34),
(14, 10, 1, 6, 41, 196.78),
(15, 1, 10, 18, 27, 175.35),
(16, 7, 6, 16, 46, 160.93),
(17, 10, 10, 11, 47, 149.01),
(18, 9, 13, 21, 13, 158.12),
(19, 7, 2, 2, 19, 191.43),
(20, 4, 8, 1, 20, 260.44);