package com.geekhub.hw8.task1;

import com.geekhub.hw8.task1.objects.Customer;
import com.geekhub.hw8.task1.objects.Product;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Application {
    public static void main(String[] args) throws SQLException {
        Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/task1",
                "postgres", "postgres");
        Statement stmt = connection.createStatement();
        stmt.executeUpdate("DELETE FROM \"order\" WHERE id > 0");
        stmt.executeUpdate("DELETE FROM \"customer\" WHERE id > 0");
        stmt.executeUpdate("DELETE FROM \"product\" WHERE id > 0");
        for (int i = 1; i < 10; i++) {
            Customer customer = new Customer();
            Product product = new Product();
            customer.setId(i);
            customer.setFirstName("CustomerName" + i);
            customer.setLastName("CustomerLastName" + i);
            customer.setCellPhone("093000" + (i * 3));
            product.setId(i);
            product.setName("ProductName" + i);
            product.setDescription("Description" + i);
            product.setCurrentPrice(i * 3.14);
            stmt.executeUpdate("INSERT INTO \"customer\" (id, firstname, lastname, cellphone) VALUES (" +
                    customer.getId() + ", '" + customer.getFirstName() + "', '" + customer.getLastName() + "', '" +
                    customer.getCellPhone() + "')");
            stmt.executeUpdate("INSERT INTO \"product\" (id, name, description, currentprice) VALUES (" +
                    product.getId() + ", '" + product.getName() + "', '" + product.getDescription() + "', " +
                    product.getCurrentPrice() + ")");
            stmt.executeUpdate("INSERT INTO \"order\" (id, customerid, productid, productquantity, deliveryplace)" +
                    " VALUES (" + i + ", " + customer.getId() + ", " + product.getId() + ", " + i + ", 'deliveryPlace" +
                    i + "')");
        }
    }
}
