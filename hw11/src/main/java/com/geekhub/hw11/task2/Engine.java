package com.geekhub.hw11.task2;

import org.springframework.stereotype.Component;

@Component
public class Engine {

    private int capacity = 2;

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return "Engine{" + "capacity=" + capacity + '}';
    }
}
