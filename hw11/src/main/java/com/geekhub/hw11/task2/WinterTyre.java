package com.geekhub.hw11.task2;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class WinterTyre extends Tyre {

    private int size;
    private String name;

    @Override
    public int getSize() {
        return size;
    }

    @Override
    @Value("16")
    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    @Value("PirelliWinter")
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Winter Tyre: {" + "size=" + size + ", name='" + name + '\'' + '}';
    }
}

