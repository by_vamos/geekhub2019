package com.geekhub.hw11.task2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class Wheel {

    private Tyre tyre;

    public Tyre getTyre() {
        return tyre;
    }

    @Autowired
    @Qualifier("winterTyre")
    public void setTyre(Tyre tyre) {
        this.tyre = tyre;
    }

    @Override
    public String toString() {
        return "It's wheel with " + tyre + "tyre";
    }
}
