package com.geekhub.hw11.task2;

import org.springframework.stereotype.Component;

@Component
public class Tyre {

    private int size = 16;
    private String name = "Pirelli";

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Tyre{" + "size=" + size + ", name='" + name + '\'' + '}';
    }
}
