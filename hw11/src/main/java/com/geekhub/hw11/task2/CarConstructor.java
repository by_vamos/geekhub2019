package com.geekhub.hw11.task2;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class CarConstructor {

    public static void main(String[] args) {

        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext("com.geekhub.hw11.task2");

        Car car = context.getBean(Car.class);
        String s = car.toString();
        System.out.println(s);

        context.close();
    }
}
