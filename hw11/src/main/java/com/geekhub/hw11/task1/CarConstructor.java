package com.geekhub.hw11.task1;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CarConstructor {

    public static void main(String[] args) {

        ClassPathXmlApplicationContext context =
                new ClassPathXmlApplicationContext(new String[] {"task1/applicationContext.xml"});
        Engine engine = context.getBean("engine", Engine.class);
        System.out.println(engine.toString());
        Tyre tyre = context.getBean("winterTyre", Tyre.class);
        System.out.println(tyre.toString());
        Wheel wheel = context.getBean("wheel", Wheel.class);;
        System.out.println(wheel.toString());
        Object car = context.getBean("car");
        System.out.println(car.toString());
        context.close();
    }
}
