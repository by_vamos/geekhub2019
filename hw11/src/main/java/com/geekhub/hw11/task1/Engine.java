package com.geekhub.hw11.task1;

public class Engine {

    private int capacity;

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return "Engine{" + "capacity=" + capacity + '}';
    }
}
