package com.geekhub.hw11.task1;

public class Wheel {

    private Tyre tyre;

    public Tyre getTyre() {
        return tyre;
    }

    public void setTyre(Tyre tyre) {
        this.tyre = tyre;
    }

    @Override
    public String toString() {
        return "It's wheel with " + tyre;
    }
}
