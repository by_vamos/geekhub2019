package com.geekhub.hw11.task1;

import org.springframework.beans.factory.annotation.Qualifier;

@Qualifier("Winter")
public class WinterTyre extends Tyre {

    private int size;
    private String name;

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Winter Tyre: {" + "size=" + size + ", name='" + name + '\'' + '}';
    }
}

