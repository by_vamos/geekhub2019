package com.geekhub.hw11.task3;

import org.springframework.beans.factory.annotation.Value;

public class Tyre {

    @Value("${tyre.size}")
    private int size;
    @Value("${tyre.name}")
    private String name;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Tyre{" + "size=" + size + ", name='" + name + '\'' + '}';
    }
}
