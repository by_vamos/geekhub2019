package com.geekhub.hw11.task3;

import org.springframework.context.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Configuration
@PropertySource("classpath:car.properties")
public class SpringConfig {

    @Bean
    public Engine engine() {
        return new Engine();
    }

    @Bean
    public Tyre tyre() {
        return new Tyre();
    }

    @Bean
    public SummerTyre summerTyre() {
        return new SummerTyre();
    }

    @Bean
    public WinterTyre winterTyre() {
        return new WinterTyre();
    }

    @Bean
    @Scope("prototype")
    public Wheel wheel() {
        Wheel wheel = new Wheel();
        wheel.setTyre(winterTyre());
        return wheel;
    }

    @Bean
    public Car car() {
        Car car = new Car();
        car.setEngine(engine());
        List<Wheel> wheels = new ArrayList<>();
        for (int i = 0; i < 4 ; i++) {
            wheels.add(wheel());
        }
        car.setWheels(wheels);
        return car;
    }
}
