package com.geekhub.hw11.task3;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class CarConstructor {

    public static void main(String[] args) {

        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(SpringConfig.class);

        Car car = context.getBean(Car.class);
        System.out.println(car.toString());

        context.close();
    }
}
