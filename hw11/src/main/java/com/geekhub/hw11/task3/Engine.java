package com.geekhub.hw11.task3;

import org.springframework.beans.factory.annotation.Value;

public class Engine {

    @Value("${engine.capacity}")
    private int capacity;

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    @Override
    public String toString() {
        return "Engine{" + "capacity=" + capacity + '}';
    }
}
