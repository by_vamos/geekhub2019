package com.geekhub.hw11.task3;

import org.springframework.beans.factory.annotation.Value;

public class SummerTyre extends Tyre {

    @Value("${summerTyre.size}")
    private int size;
    @Value("${summerTyre.name}")
    private String name;

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Summer Tyre: {" + "size=" + size + ", name='" + name + '\'' + '}';
    }
}
