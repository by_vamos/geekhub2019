package com.geekhub.hw11.task3;

import org.springframework.beans.factory.annotation.Value;

public class WinterTyre extends Tyre {

    @Value("${winterTyre.size}")
    private int size;
    @Value("${winterTyre.name}")
    private String name;

    @Override
    public int getSize() {
        return size;
    }

    @Override
    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Winter Tyre: {" + "size=" + size + ", name='" + name + '\'' + '}';
    }
}

