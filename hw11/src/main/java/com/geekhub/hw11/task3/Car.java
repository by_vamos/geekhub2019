package com.geekhub.hw11.task3;

import java.util.List;

public class Car {

    private Engine engine;
    private List<Wheel> wheels;

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public List<Wheel> getWheels() {
        return wheels;
    }

    public void setWheels(List<Wheel> wheels) {
        this.wheels = wheels;
    }

    @Override
    public String toString() {
        return "Car{" + "engine=" + engine + ", wheels=" + wheels + '}';
    }
}
