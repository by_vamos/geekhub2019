package com.geekhub.hw9.task2;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

/**
 * Represents worker that downloads image from URL to specified folder.<br/>
 * Name of the image will be constructed based on URL. Names for the same URL will be the same.
 */
public class ImageTask implements Runnable {

    private final URL url;
    private final String folder;

    /**
     * Inherited method that do main job - downloads the image and stores it at specified location
     */
    public ImageTask(URL url, String folder) {
        this.url = url;
        this.folder = folder;
    }

    /**
     * Inherited method that do main job - downloads the image and stores it at specified location
     */
    @Override
    public void run() {
        //TODO: done
        //This method downloads image from the specified url and saves the image to the given folder
        String fileName = buildFileName(url);
        File file = new File(folder + "/" + fileName);
        try {
            BufferedImage image = ImageIO.read(url);
            ImageIO.write(image, "jpg", file);
        } catch (FileNotFoundException e) {
            try {
                file.createNewFile();
            } catch (IOException ignored) {
            }
        } catch (IOException ignored) {
        }

    }

    //converts URL to unique file name
    private String buildFileName(URL url) {
        return url.toString().replaceAll("[^a-zA-Z0-9-_.]", "_");
    }
}
