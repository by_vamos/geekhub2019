package com.geekhub.hw9.task2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class ConnectionUtils {

    private ConnectionUtils() {
    }

    public static byte[] getData(URL url) throws IOException {
        //TODO: done
        URLConnection urlConnection = url.openConnection();
        InputStream inputStream = urlConnection.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        String htmlContent;
        StringBuilder content = new StringBuilder();
        while ((htmlContent = br.readLine()) != null) {
            content.append(htmlContent);
        }
        return content.toString().getBytes();
        //Must download all content of the specified url as byte array
    }
}
