package com.geekhub.hw9.task1;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Application1 {
    public static void main(String[] args) throws IOException, InterruptedException {
        ExecutorService taskExecutor = Executors.newFixedThreadPool(5);
        File sources = new File("hw9/src/main/java/com/geekhub/hw9/task1/References.txt");
        File result = new File("hw9/src/main/java/com/geekhub/hw9/task1/Result.txt");
        List<String> resultlist = new ArrayList<>();
        List<String> urlList = createUrlList(sources);
        for (String urlAddress : urlList) {
            taskExecutor.execute(() -> {
                URL url = null;
                try {
                    url = new URL(urlAddress);
                } catch (MalformedURLException ignored) {
                }
                URLConnection urlConnection = null;
                try {
                    urlConnection = url.openConnection();
                } catch (IOException ignored) {
                }
                InputStream inStream = null;
                try {
                    inStream = urlConnection.getInputStream();
                } catch (IOException ignored) {
                }
                StringBuilder content = new StringBuilder();
                try {
                    try (BufferedReader br = new BufferedReader(new InputStreamReader(inStream))) {
                        String htmlContent = null;
                        while (true) {
                            try {
                                if ((htmlContent = br.readLine()) == null) break;
                            } catch (IOException ignored) {
                            }
                            content.append(htmlContent);
                        }
                        try {
                            br.close();
                        } catch (IOException ignored) {
                        }
                    }
                } catch (IOException ignored) {
                }
                String str = content.toString();
                MessageDigest md5 = null;
                try {
                    md5 = MessageDigest.getInstance("MD5");
                } catch (NoSuchAlgorithmException ignored) {
                }
                byte[] bytes = md5.digest(str.getBytes());
                StringBuilder builder = new StringBuilder();
                for (byte aByte : bytes) {
                    builder.append(String.format("%02X", aByte));
                }
                System.out.println(builder.toString());
                resultlist.add(builder.toString());
            });
        }
        Thread.currentThread().join();
        addResultInFile(resultlist, result);
        taskExecutor.shutdown();
    }

    private static List<String> createUrlList(File sources) throws IOException {
        List<String> urlList = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(sources))) {
            String s;
            while ((s = reader.readLine()) != null)
                urlList.add(s);
            return urlList;
        }
    }

    private static void addResultInFile(List<String> resultlist, File res) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(res))) {
            for (String s : resultlist) {
                writer.write(s);
                writer.newLine();
            }
            writer.flush();
        }
    }
}
