package com.geekhub.hw4.linked;

import com.geekhub.hw4.List;

import java.util.Iterator;

public class LinkedList<E> implements List<E> {

    private Node<E> head;
    private Node<E> tail;

    private int size = 0;

    @Override
    public boolean add(E element) {
        Node<E> node = new Node<>(element, null);
        if (tail == null) {
            head = node;
            tail = node;
        } else {
            tail.next = node;
            tail = node;
        }
        size++;
        return true;
    }

    @Override
    public boolean add(int index, E element) {
        if (index < 0 || index > size()) {
            throw new IndexOutOfBoundsException();
        }
        Node<E> node = new Node<>(element, null);
        Node<E> temp = head;
        Node<E> current = null;
        if (index == 0) {
            head = node;
            head.next = temp;
            size++;
            return true;
        }
        int i = 0;
        while (i < index) {
            current = temp;
            temp = temp.next;
            i++;
        }
        node.next = current.next;
        current.next = node;
        size++;
        return true;
    }

    @Override
    public boolean addAll(List<E> elements) {
        for (E element : elements) {
            Node<E> node = new Node<>(element, null);
            tail.next = node;
            tail = node;
            size++;
        }
        return true;
    }

    @Override
    public boolean addAll(int index, List<E> elements) {
        if (index < 0 || index > size()) {
            throw new IndexOutOfBoundsException();
        }
        if (elements.isEmpty())
            return false;
        Node<E> first = new Node<>(elements.get(0), null);
        Node<E> finalFirst = first;
        for (E element : elements) {
            first.next = new Node<>(element, null);
            first = first.next;
        }
        first = finalFirst.next;
        while (first.next != null) {
            first = first.next;
        }
        Node<E> last = first;
        first = finalFirst.next;
        if (index == 0) {
            last.next = head;
            head = first;
            size += elements.size();
            return true;
        }
        int i = 0;
        Node<E> temp = head;
        Node<E> preIndexedElement = null;
        while (i < index) {
            preIndexedElement = temp;
            temp = temp.next;
            i++;
        }
        preIndexedElement.next = first;
        last.next = temp;
        size += elements.size();
        return true;
    }

    @Override
    public boolean clear() {
        head = null;
        tail = null;
        size = 0;
        return true;
    }

    @Override
    public E remove(int index) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        E item = null;
        Node<E> temp = head;
        Node<E> preIndexedElement = null;
        if (index == 0) {
            item = head.element;
            head = head.next;
            size--;
            return item;
        }
        int i = 0;
        while (i < index) {
            preIndexedElement = temp;
            temp = temp.next;
            item = temp.element;
            i++;
        }
        preIndexedElement.next = temp.next;
        if (index == size - 1) {
            tail = preIndexedElement;
        }
        size--;
        return item;
    }

    @Override
    public E remove(E element) {
        E item;
        Node<E> node = head;
        Node<E> preIndexedElement;
        if (head.element.equals(element)) {
            item = head.element;
            head.element = null;
            head = head.next;
            size--;
            return item;
        }
        int i = 0;
        while (i < indexOf(tail.element)) {
            preIndexedElement = node;
            node = node.next;
            if (node.element.equals(element)) {
                item = node.element;
                node.element = null;
                assert false;
                preIndexedElement.next = node.next;
                size--;
                return item;
            }
            i++;
        }
        return null;
    }

    @Override
    public E get(int index) {
        if (index < 0 || index >= size()) {
            throw new IndexOutOfBoundsException();
        }
        E item;
        Node<E> node = head;
        if (index == 0) {
            return head.element;
        }
        int i = 0;
        while (i < index) {
            node = node.next;
            i++;
        }
        item = node.element;
        return item;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null && tail == null;
    }

    @Override
    public int indexOf(E element) {
        Node<E> current = head;
        int index = 0;
        while (current != null) {
            if (current.element.equals(element)) {
                return index;
            }
            index++;
            current = current.next;
        }
        return -1;
    }

    @Override
    public boolean contains(E element) {
        Node<E> node = head;
        if (head.element.equals(element)) {
            return true;
        }
        while (!(tail.element == null)) {
            node = node.next;
            if (node.element.equals(element)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return new LinkedListIterator<>(head);
    }
}
