package com.geekhub.hw4.linked;

import java.util.Objects;

class Node<E> {
    E element;
    Node<E> next;

    Node(E element, Node<E> next) {
        this.element = element;
        this.next = next;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node<?> node = (Node<?>) o;
        return element.equals(node.element) &&
                next.equals(node.next);
    }

    @Override
    public int hashCode() {
        return Objects.hash(element, next);
    }
}
