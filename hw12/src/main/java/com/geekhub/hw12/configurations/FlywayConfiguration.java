package com.geekhub.hw12.configurations;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class FlywayConfiguration {

    private final DataSource datasource;

    @Autowired
    public FlywayConfiguration(DataSource datasource) {
        this.datasource = datasource;
    }

    @Bean
    public Flyway flyway() {
        Flyway flyway = Flyway.configure().dataSource(datasource).locations("").load();
        flyway.migrate();
        return flyway;
    }
}
