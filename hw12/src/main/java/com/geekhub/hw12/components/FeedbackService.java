package com.geekhub.hw12.components;

import com.geekhub.hw12.entities.Feedback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class FeedbackService {

    private final FeedBackRepository feedBackRepository;

    @Autowired
    public FeedbackService(FeedBackRepository feedBackRepository) {
        this.feedBackRepository = feedBackRepository;
    }

    public void addDataIntoDb(String name, String message, String rating) throws SQLException {
        String time = LocalTime.now().format(DateTimeFormatter.ofPattern("hh:mm:ss")) + " " +
                LocalDate.now().getMonth() + " " + LocalDate.now().getDayOfMonth();
        Feedback feedback = new Feedback();
        feedback.setDate(time);
        feedback.setName(name);
        feedback.setMessage(message);
        feedback.setRating(rating);
        if (!(name == null && message == null && rating == null)) {
            feedBackRepository.addDataIntoDb(feedback);
        }
    }

    public List<Feedback> getAllDataFromDb() throws SQLException {
        return feedBackRepository.getAllDataFromDb();
    }
}
