package com.geekhub.hw12.components;

import com.geekhub.hw12.entities.Feedback;
import com.geekhub.hw12.entities.FeedbackDto;
import org.springframework.stereotype.Component;

@Component
public class FeedbackConverter {

    public FeedbackDto convert(Feedback feedback) {
        FeedbackDto feedbackDto = new FeedbackDto();
        feedbackDto.setDate(feedback.getDate());
        feedbackDto.setName(feedback.getName());
        feedbackDto.setMessage(feedback.getMessage());
        feedbackDto.setRating(feedback.getRating());
        return feedbackDto;
    }
}
