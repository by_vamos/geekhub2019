package com.geekhub.hw12.components;

import com.geekhub.hw12.entities.Feedback;
import com.geekhub.hw12.entities.FeedbackDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class FeedbackController {

    private final FeedbackService feedBackService;
    private final FeedbackConverter feedbackConverter;

    @Autowired
    public FeedbackController(FeedbackService feedBackService, FeedbackConverter feedbackConverter) {
        this.feedBackService = feedBackService;
        this.feedbackConverter = feedbackConverter;
    }

    @PostMapping("/guest-book-spring")
    public String form(@RequestParam(required = false) String name,
                     @RequestParam(required = false) String message,
                     @RequestParam(required = false) String rating) throws SQLException {
        feedBackService.addDataIntoDb(name, message, rating);
        return "redirect:/guest-book-spring";
    }

    @GetMapping("guest-book-spring")
    public String form(ModelMap model) throws SQLException {
        ArrayList<FeedbackDto> feedbackDtosList = new ArrayList<>();
        List<Feedback> allDataFromDataBase = feedBackService.getAllDataFromDb();
        for (Feedback feedback : allDataFromDataBase) {
            FeedbackDto feedbackDto = feedbackConverter.convert(feedback);
            feedbackDtosList.add(feedbackDto);
        }
        model.addAttribute("result", feedbackDtosList);
        return "form";
    }
}
