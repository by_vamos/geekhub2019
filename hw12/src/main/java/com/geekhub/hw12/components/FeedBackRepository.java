package com.geekhub.hw12.components;

import com.geekhub.hw12.entities.Feedback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class FeedBackRepository {
    private final List<Feedback> databaseRecordsList = new ArrayList<Feedback>();
    private final DataSource dataSource;

    @Autowired
    public FeedBackRepository(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private Connection createConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public void addDataIntoDb(Feedback feedback) throws SQLException {
        try (Connection conn = createConnection()) {
            try (PreparedStatement prpstmt = conn.prepareStatement("INSERT INTO feedbacks(datetime, name, message, rating)" +
                    " VALUES (?, ?, ?, ?)")) {
                prpstmt.setString(1, feedback.getDate());
                prpstmt.setString(2, feedback.getName());
                prpstmt.setString(3, feedback.getMessage());
                prpstmt.setString(4, feedback.getRating());
                prpstmt.executeUpdate();
            }
        }
    }

    public List<Feedback> getAllDataFromDb() throws SQLException {
        try (Connection conn = createConnection()) {
            ResultSet resultSet;
            try (Statement stmt = conn.createStatement()) {
                resultSet = stmt.executeQuery("SELECT datetime, name, message, rating FROM feedbacks");
                while (resultSet.next()) {
                    Feedback feedback = new Feedback();
                    feedback.setDate(resultSet.getString("datetime"));
                    feedback.setName(resultSet.getString("name"));
                    feedback.setMessage(resultSet.getString("message"));
                    feedback.setRating(resultSet.getString("rating"));
                    databaseRecordsList.add(0, feedback);
                }
            }
        }
        return databaseRecordsList;
    }
}
