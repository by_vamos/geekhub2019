CREATE TABLE feedbacks
(
    id SERIAL PRIMARY KEY,
    datetime VARCHAR NOT NULL,
    name VARCHAR,
    message VARCHAR,
    rating VARCHAR
)