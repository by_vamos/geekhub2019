public class Square implements Shape {
    private final int a;
    private final Color color;

    Square() {
        this.a = (int) (1 + (Math.random() * 10));
        this.color = Color.randomColor();
    }

    @Override
    public double calculateArea() {
        return a * a;
    }

    @Override
    public double calculatePerimeter() {
        return 4 * a;
    }

    @Override
    public String toString() {
        return "Shape: Square, " +
                "a=" + a +
                ", color=" + color +
                ", area=" + calculateArea() +
                ", perimeter=" + calculatePerimeter();
    }
}