import java.util.Random;

public enum Color {
    YELLOW, WHITE, BLACK, RED, GREEN, BLUE, ORANGE, PINK, PURPLE, TERRACOTTA;

    public static Color randomColor() {
        return Color.values()[(new Random().nextInt(Color.values().length))];
    }
}