public class Trapeze implements Shape {
    private final int a;
    private final int b;
    private final int c;
    private final int d;
    private final Color color;

    public Trapeze() {
        this.a = (int) (1 + Math.random() * 10);
        this.b = (int) (1 + Math.random() * 10);
        this.c = (int) (1 + Math.random() * 10);
        this.d = (int) (1 + Math.random() * 10);
        this.color = Color.randomColor();
    }

    @Override
    public double calculateArea() {
        double abbreviationFormula = square(a) - 2 * (a * b) + square(b);
        return (double) ((a + b) / 2) * Math.sqrt((square(c)) -
                square((abbreviationFormula + square(c) + square(d)) / (2 * (a - b))));
    }

    private double square(double number) {
        return number * number;
    }

    @Override
    public double calculatePerimeter() {
        return a + b + c + d;
    }

    @Override
    public String toString() {
        return "Shape: Trapeze, " +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                ", d=" + d +
                ", color=" + color +
                ", area=" + calculateArea() +
                ", perimeter=" + calculatePerimeter();
    }
}
