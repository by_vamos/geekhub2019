import java.util.Random;

public enum AvailableShape {
    CIRCLE,
    TRIANGLE,
    SQUARE,
    TRAPEZE;

    public static AvailableShape randomShape() {
        return AvailableShape.values()[(new Random().nextInt(AvailableShape.values().length))];
    }
}