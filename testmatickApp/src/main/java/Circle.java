public class Circle implements Shape {
    private final double PI = 3.1415;
    private final int r;
    private final Color color;

    Circle() {
        this.r = (int) (1 + Math.random() * 10);
        this.color = Color.randomColor();
    }

    @Override
    public double calculateArea() {
        return PI * r * r;
    }

    @Override
    public double calculatePerimeter() {
        return 2 * PI * r;
    }

    @Override
    public String toString() {
        return "Shape: Circle, " +
                ", r=" + r +
                ", color=" + color +
                ", area=" + calculateArea() +
                ", perimeter=" + calculatePerimeter();
    }
}