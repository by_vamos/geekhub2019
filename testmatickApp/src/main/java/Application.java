import java.util.ArrayList;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        ArrayList<Shape> shapes = new ArrayList<>();
        System.out.println("Enter the number of shapes: ");
        Scanner sc = new Scanner(System.in);
        int size = sc.nextInt();
        for (int i = 0; i < size; i++) {
            AvailableShape availableShape = AvailableShape.randomShape();
            switch (availableShape) {
                case CIRCLE:
                    shapes.add(new Circle());
                    break;
                case SQUARE:
                    shapes.add(new Square());
                    break;
                case TRAPEZE:
                    shapes.add(new Trapeze());
                    break;
                case TRIANGLE:
                    shapes.add(new Triangle());
                    break;
                default:
                    break;
            }
        }
        for (Shape shape : shapes) {
            System.out.println(shape.toString());
        }
    }
}