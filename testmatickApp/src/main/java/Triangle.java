public class Triangle implements Shape {
    private final int a;
    private final int b;
    private final int c;
    private final Color color;

    Triangle() {
        this.a = (int) (3 + Math.random() * 10);
        this.b = (int) (4 + Math.random() * 10);
        this.c = (int) (5 + Math.random() * 10);
        this.color = Color.randomColor();
    }

    @Override
    public double calculateArea() {
        double p = (double) (a + b + c) / 2;
        return Math.sqrt(p * (p - a) * (p - b) * (p - c));
    }

    @Override
    public double calculatePerimeter() {
        return a + b + c;
    }

    @Override
    public String toString() {
        return "Shape: Triangle, " +
                "a=" + a +
                ", b=" + b +
                ", c=" + c +
                ", color=" + color +
                ", area=" + calculateArea() +
                ", perimeter=" + calculatePerimeter();
    }
}